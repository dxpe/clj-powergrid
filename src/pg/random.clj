(ns pg.random
  (:refer-clojure :exclude [rand-int rand-nth shuffle])
  (:import [java.util Random]))

(defn ^Random rng
  "Returns a random number generator"
  ([] (Random.))
  ([^long seed] (Random. seed)))

(defn set-rng-seed
  [^Random rng ^long seed]
  (.setSeed rng seed))

(defn rand-int
  "Returns the next pseudorandom, uniformly distributed int value from
  this random number generator's sequence. If no bound is provided,
  returns one int value; all 2 ^ 32 possible int values are produced
  with (approximately) equal probability. If a bound is provided,
  returns an int value between 0 (inclusive) and the bound (exclusive)."
  ([^Random rng]
     (.nextInt rng))
  ([^Random rng bound]
     (.nextInt rng bound)))

(defn rand-nth
  "Similar to clojure.core/rand-nth except that it requires a random
  number generator."
  [rng coll]
  (nth coll (rand-int rng (count coll))))

(defn shuffle
  "Shuffle collection using the given random number generator using the
  Fisher Yates algorithm. Code borrowed from clojure.data.generators."
  [rng coll]
  (let [as (object-array coll)]
    (loop [i (dec (count as))]
      (if (<= 1 i)
        (let [j (rand-int rng (inc i))
              t (aget as i)]
          (aset as i (aget as j))
          (aset as j t)
          (recur (dec i)))
        (vec as)))))
