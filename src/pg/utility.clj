(ns pg.utility
  (:require [clojure.set :as set]
            [clojure.string :as str]))

(defn ^:internal common-keys
  [a b]
  (set/intersection (set (keys a)) (set (keys b))))

(defn atom?
  [x]
  (instance? clojure.lang.Atom x))

(defn heading
  [s]
  (str "==== " s " ===="))

(defn print-msg
  "Prints a message using println."
  [& args]
  (println (str "<<<< " (apply format args) " >>>>")))

(defn invalid-cmd!
  [msg ps]
  (throw (ex-info (str "Invalid command: " msg " Possible solution: " ps)
                  {:invalid-command true})))

(defn iterate-until-stable
  "Takes a function of one arg and calls (f x), (f (f x)), and so on
  until the value does not change."
  [f x]
  (loop [v x]
    (let [v' (f v)]
      (if (= v' v)
        v
        (recur v')))))

(defn max-or-nil
  "Similar to clojure.core/max but accepts a collection and returns nil
  if nil or empty."
  [coll]
  (if (seq coll)
    (apply max coll)))

(defn rotate-left
  "Returns an 'updated' vector, rotated one position to the left."
  [v]
  {:pre [(vector? v)]}
  (conj (subvec v 1) (first v)))

(defn safe-merge
  "Same as merge if all keys are unique; otherwise throws an exception."
  [a b]
  (let [merged (merge a b)]
    (if (= (count merged) (+ (count a) (count b)))
      merged
      (let [ks (common-keys a b)]
        (throw (ex-info (str "Duplicate keys: " (str/join "" ks))
                        {:keys ks}))))))

(defn not-implemented
  [s]
  (println "* not implemented :" s "*"))
