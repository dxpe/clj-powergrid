(ns pg.grids.usa
  "A grid (map) of the USA with these regions:
  1. mostly northwest
  2. mostly midwest
  3. northeast
  4. mostly southeast
  5. most of the south
  6. most of the southwest")

(def regions-by-city
  "A map where keys are cities and values are regions."
  {:billings       1
   :boise          1
   :cheyenne       1
   :denver         1
   :omaha          1
   :portland       1
   :seattle        1
   :chicago        2
   :cincinatti     2
   :duluth         2
   :fargo          2
   :knoxville      2
   :minneapolis    2
   :st-louis       2
   :boston         3
   :buffalo        3
   :detroit        3
   :new-york       3
   :philadelphia   3
   :pittsburgh     3
   :washington     3
   :atlanta        4
   :jacksonville   4
   :miami          4
   :norfolk        4
   :raleigh        4
   :savannah       4
   :tampa          4
   :birmingham     5
   :dallas         5
   :houston        5
   :kansas-city    5
   :memphis        5
   :new-orleans    5
   :oklahoma-city  5
   :las-vegas      6
   :los-angeles    6
   :phoenix        6
   :salt-lake-city 6
   :san-diego      6
   :san-francisco  6
   :santa-fe       6})

(def regions
  "A set of regions (numbers)."
  (-> regions-by-city vals set))

(def connection-costs
  "A map where keys are sets (each containing two cities) and values are
  connection costs."
  {;; region 1
   #{:seattle       :portland}        3
   #{:seattle       :billings}        9
   #{:seattle       :boise}          12
   #{:portland      :boise}          13
   #{:boise         :billings}       12
   #{:boise         :cheyenne}       24
   #{:cheyenne      :denver}          0
   #{:cheyenne      :billings}        9
   #{:cheyenne      :omaha}          14
   ;; region 1 <-> 2
   #{:billings      :fargo}          17
   #{:billings      :minneapolis}    18
   #{:cheyenne      :minneapolis}    18
   #{:omaha         :minneapolis}     8
   #{:omaha         :chicago}        13
   ;; region 1 <-> 5
   #{:denver        :kansas-city}    16
   #{:omaha         :kansas-city}     5
   ;; region 1 <-> 6
   #{:portland      :san-francisco}  24
   #{:boise         :san-francisco}  23
   #{:boise         :salt-lake-city}  8
   #{:denver        :salt-lake-city} 21
   #{:denver        :santa-fe}       13
   ;; region 2
   #{:duluth        :chicago}        12
   #{:duluth        :minneapolis}     5
   #{:duluth        :fargo}           6
   #{:fargo         :minneapolis}     6
   #{:chicago       :minneapolis}     8
   #{:chicago       :st-louis}       10
   #{:chicago       :cincinatti}      7
   #{:st-louis      :cincinatti}     12
   #{:knoxville     :cincinatti}      6
   ;; region 2 <-> 3
   #{:duluth        :detroit}        15
   #{:chicago       :detroit}         7
   #{:cincinatti    :detroit}         4
   #{:cincinatti    :pittsburgh}      7
   ;; region 2 <-> 4
   #{:cincinatti    :raleigh}        15
   #{:knoxville     :atlanta}         5
   #{:st-louis      :atlanta}        12
   ;; region 2 <-> 5
   #{:st-louis      :memphis}         7
   #{:st-louis      :kansas-city}     6
   #{:chicago       :kansas-city}     8
   ;; region 3
   #{:detroit       :buffalo}         7
   #{:detroit       :pittsburgh}      6
   #{:pittsburgh    :buffalo}         7
   #{:pittsburgh    :washington}      6
   #{:buffalo       :new-york}        8
   #{:washington    :philadelphia}    3
   #{:philadelphia  :new-york}        0
   #{:new-york      :boston}          3
   ;; region 3 <-> 4
   #{:washington    :norfolk}         5
   #{:pittsburgh    :raleigh}         7
   ;; region 4
   #{:norfolk       :raleigh}         3
   #{:raleigh       :atlanta}         7
   #{:raleigh       :savannah}        7
   #{:atlanta       :savannah}        7
   #{:savannah      :jacksonville}    0
   #{:jacksonville  :tampa}           4
   #{:tampa         :miami}           4
   ;; region 4 <-> 5
   #{:birmingham    :jacksonville}    9
   #{:jacksonville  :new-orleans}    16
   #{:atlanta       :birmingham}      3
   ;; region 5
   #{:kansas-city   :oklahoma-city}   8
   #{:kansas-city   :memphis}        12
   #{:oklahoma-city :memphis}        14
   #{:oklahoma-city :dallas}          3
   #{:dallas        :memphis}        12
   #{:dallas        :new-orleans}    12
   #{:memphis       :new-orleans}     7
   #{:dallas        :houston}         5
   #{:houston       :new-orleans}     8
   #{:birmingham    :new-orleans}    11
   #{:birmingham    :memphis}         6
   ;; region 5 <-> 6
   #{:kansas-city   :santa-fe}       16
   #{:oklahoma-city :santa-fe}       15
   #{:dallas        :santa-fe}       16
   #{:houston       :santa-fe}       21
   ;; region 6
   #{:san-francisco :salt-lake-city} 27
   #{:san-francisco :las-vegas}      14
   #{:san-francisco :los-angeles}     9
   #{:los-angeles   :las-vegas}       9
   #{:los-angeles   :san-diego}       3
   #{:san-diego     :las-vegas}       9
   #{:san-diego     :phoenix}        14
   #{:las-vegas     :salt-lake-city} 18
   #{:las-vegas     :santa-fe}       27
   #{:las-vegas     :phoenix}        15
   #{:phoenix       :santa-fe}       18})
