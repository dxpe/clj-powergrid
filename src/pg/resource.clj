(ns pg.resource
  (:require
   [clojure.math.combinatorics :as combo]
   [pg.constants.resource :refer [empty-resource-map]]
   [pg.utility :refer :all]))

;; == options ==

(defn- hybrid-option
  [[c o]]
  (let [a (if (pos? c) {:coal c} {})
        b (if (pos? o) {:oil  o} {})]
    (merge a b)))

(defn- hybrid-options
  [quantity]
  {:pre [(pos? quantity)]}
  (let [r (range (inc quantity))]
    (->> (map list (reverse r) r)
         (mapv hybrid-option))))

(defn resource-options
  [resource quantity]
  (case resource
    :coal    [{:coal    quantity}]
    :oil     [{:oil     quantity}]
    :garbage [{:garbage quantity}]
    :uranium [{:uranium quantity}]
    :hybrid  (hybrid-options quantity)))

;; == expand and collapse ==

(defn expand-resource-map
  "Returns a map with all resources, even if quantity is zero."
  [resource-map]
  {:pre [(map? resource-map)]}
  (merge empty-resource-map resource-map))

(defn collapse-resource-map
  "Returns a map with only non-zero resources."
  [resource-map]
  {:pre [(map? resource-map)]}
  (into {} (remove (fn [[r q]] (zero? q)) resource-map)))

(defn positive-quantity-resource-map
  "Returns a map with only positive resources."
  [resource-map]
  {:pre [(map? resource-map)]}
  (into {} (filter (fn [[r q]] (pos? q)) resource-map)))

;; == add and subtract ==

(defn- merge-resource-maps-with
  [f maps]
  (if (or (nil? maps) (empty? maps))
    {}
    (->> (map expand-resource-map maps)
         (apply merge-with f)
         collapse-resource-map)))

(defn add-resource-maps
  [& maps]
  (merge-resource-maps-with + maps))

(defn subtract-resource-maps
  [& maps]
  (merge-resource-maps-with - maps))

(defn min-resource-maps
  "Merge resource maps, returns the minimum value for each key."
  [& maps]
  (merge-resource-maps-with min maps))

;; == excess ==

(defn excess-of
  "Returns how much resource map a is in excess of resource map b. In
  other words, returns how many resources would need to be discarded
  from resource-a to 'fit into' (be a subset of) resources-b."
  [a b]
  (positive-quantity-resource-map (subtract-resource-maps a b)))

;; == validity ==

(defn valid-resource-map?
  "Returns true if all quantities are 0 or greater."
  [resource-map]
  {:pre [(map? resource-map)]}
  (every? #(>= % 0) (vals resource-map)))

(defn valid-resource-maps
  "Remove invalid resource maps from a sequence."
  [resource-maps]
  (filter valid-resource-map? resource-maps))

;; == market availability ==

(defn available-resource-map?
  "Is a resource map able to be purchased?"
  [{:keys [resources] :as state} resource-map]
  (-> (subtract-resource-maps resources resource-map)
      valid-resource-map?))

(defn available-resource-maps
  "Remove unavailable resource maps from a sequence."
  [state resource-maps]
  (filter #(available-resource-map? state %) resource-maps))

;; == in inventory ==

(defn resources-in-inventory?
  "Does the player (the current player in the 2-arity version or the
  specified player in the 3-arity version) have (at least) the specified
  resources in their inventory?"
  ([{:keys [resources-by-player] :as state} player resource-map]
     (-> (resources-by-player player)
         (subtract-resource-maps resource-map)
         valid-resource-map?))
  ([{:keys [player] :as state} resource-map]
     (resources-in-inventory? state player resource-map)))

;; == in-use / in the ground ==

(defn resources-in-use
  "Returns a resource map of resources either in the
  market or kept in player inventory."
  [{:keys [resources resources-by-player] :as state}]
  (apply add-resource-maps resources (vals resources-by-player)))

(defn resources-in-ground
  "Returns a resource map of resources 'in the ground', meaning that
  these are available during the resupply phase."
  [{:keys [max-resources] :as state}]
  (let [in-use (resources-in-use state)]
    (subtract-resource-maps max-resources in-use)))

(defn resources-to-resupply
  "Returns the proper amount of resources to resupply. Should only be
  used in the resource resupply phase."
  [{:keys [players resource-supply step] :as state}]
  {:pre [players]}
  (let [ground-res (resources-in-ground state)
        k (count players)
        resupply (get-in resource-supply [k step])]
    (min-resource-maps ground-res resupply)))

;; == cost ==

(defn resource-cost
  "Returns the cost of buying the next (cheapest) available
  resource. Returns nil if there are no resources available for
  purchase."
  [{:keys [resource-costs resources] :as state} resource]
  (if-let [quantity (resources resource)]
    ((resource-costs resource) quantity)))

(defn resources-cost
  "Returns the cost of buying q resources. Returns nil if all of the
  resources cannot be purchased."
  [{:keys [resource-costs resources] :as state} resource quantity]
  (loop [total 0 q quantity rs resources]
    (if (zero? q)
      total
      (if-let [cost (resource-cost state resource)]
        (let [rs' (subtract-resource-maps rs {resource 1})]
          (recur (+ total cost) (dec q) rs'))))))

(defn resource-map-cost
  "Returns the cost of buying the resource-map. Returns nil if all of
  the resources cannot be purchased."
  [{:keys [resource-costs resources] :as state} resource-map]
  (let [costs (for [[r q] resource-map]
                (resources-cost state r q))]
    (if (every? identity costs)
      (reduce + costs))))

(defn resource-maps-for-cost
  "Returns the resource maps that are no greater than the provided cost."
  [{:keys [resource-costs resources] :as state} cost resource-maps]
  (filter #(<= (resource-map-cost state %) cost) resource-maps))

;; == subsets ==

(defn resource-map-subsets
  "Returns all 'lesser or equal' resource-maps. For example, if given {:oil 2}
  this function will retun [{:oil 1} {:oil 2}]."
  [resource-map]
  (let [res-map (expand-resource-map resource-map)
        quantities (map res-map pg.constants.resource/resources)
        ranges (map #(range (inc %)) quantities)]
    (->> (apply combo/cartesian-product ranges)
         (map #(zipmap pg.constants.resource/resources %))
         (map collapse-resource-map)
         (remove empty?))))

(defn resource-map-options-subsets
  "Returns a sequence of options. Each returned option is a resource map
  that is 'less than or equal to' from an option in the input."
  [resource-map-options]
  (distinct (mapcat resource-map-subsets resource-map-options)))
