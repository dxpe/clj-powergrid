(ns pg.city
  (:require
   [clojure.set :as set]
   [dijkstra.core :refer [dijkstra]]
   [pg.constants.city :refer [base-city-connection-cost
                              max-city-connections]]
   [pg.utility :as u]))

(defn union-neighbor-cities
  "Returns the union of the provided initial cities and their neighbors."
  [{:keys [connections] :as state} initial-cities]
  (->> connections
       (remove #(empty? (set/intersection initial-cities %)))
       (apply concat)
       set))

(defn union-connected-neighbors
  "Returns the union of the provided initial cities and their
  neighboring cities that are connected to any power grid. This function
  only returns neighbors that are one-hop out."
  [{:keys [cities-by-player connections] :as state} initial-cities]
  (let [neighbors (union-neighbor-cities state initial-cities)
        powered (apply set/union (vals cities-by-player))]
    (set/intersection neighbors powered)))

(defn iterate-union-connected-neighbors
  "Returns the union of the provided initial cities and their
  neighboring connected cities (powered by some player), iteratively
  until the set is stable (i.e. does not grow)."
  [state cities]
  (u/iterate-until-stable #(union-connected-neighbors state %) cities))

(defn connected-cities
  "Returns a set of a player's indirectly or directly connected
  cities. This function is useful building block for later functions
  that calculate cities one-hop away, since those are cities that are
  eligible for expansion. The basic procedure works as follows:
  1. start with all possible connections
  2. remove connections not connected to a player's cities
  3. convert from connections to cities (flatten, distinct)
  4. remove unoccupied cities"
  [{:keys [cities-by-player player players-by-city] :as state}]
  (let [cities (cities-by-player player)]
    (if (empty? cities)
      #{}
      (iterate-union-connected-neighbors state cities))))

(defn unoccupied-cities
  "Returns a set of unoccupied cities."
  [{:keys [players-by-city] :as state}]
  (->> players-by-city
       (filter (fn [[city players]] (empty? players)))
       keys set))

(defn available-cities
  "Returns a set of available cities for the current player. This
  includes cities that (a) have not exceeded the maximum number of
  connections given the current step (e.g. 1, 2, or 3) and (b) the
  current player is not already connected to."
  [{:keys [cities cities-by-player player players-by-city step] :as state}]
  {:pre [step player (set? cities)
         (map? cities-by-player) (map? players-by-city)]}
  (let [max-conns (max-city-connections step)]
    (-> (->> players-by-city
             (filter (fn [[city players]] (< (count players) max-conns)))
             keys set)
        (set/difference (player cities-by-player)))))

(defn candidate-cities
  "Returns a set of candidate cities for expansion for the current
  player. Among other things, this depends on the current step."
  [state]
  (let [conn-cities (connected-cities state)]
    (if (empty? conn-cities)
      (unoccupied-cities state)
      (->> (union-neighbor-cities state conn-cities)
           (set/intersection (available-cities state))))))

(defn candidate-city?
  "Is the city a viable candidate to build for the current player?"
  [state city]
  {:pre [(map? state) city]}
  (city (candidate-cities state)))

(defn directed-edges
  "Returns a map of {[city-1 city-2] cost} pairs suitable for calling the
  dijkstra function."
  [cost-by-connection]
  (->> cost-by-connection
       (mapcat (fn [[two-cities cost]]
                 (let [[a b] (seq two-cities)]
                   (list [[a b] cost] [[b a] cost]))))
       (into {})))

(defn relevant-connection-costs
  "Updates and returns a connections map of {#{city-1 city-2} cost}
  pairs that either (a) connect within the cities (b) connect between
  the cities and the destination city."
  [cost-by-connection cities dest]
  {:pre [(map? cost-by-connection) (set? cities)]}
  (let [relevant (fn [[two-cities cost]]
                   (let [[a b] (seq two-cities)]
                     (or (and (cities a) (cities b))
                         (and (cities a) (= b dest))
                         (and (cities b) (= a dest)))))]
    (into {} (filter relevant cost-by-connection))))

(defn city-interconnect-cost
  "Returns the least expensive 'interconnect' cost for the current
  player to connect to a city (not including the base city
  cost)."
  [{:keys [cities-by-player cost-by-connection player] :as state} city]
  (let [cities (connected-cities state)
        vertexes (conj cities city)
        edges (-> (relevant-connection-costs cost-by-connection cities city)
                  directed-edges)
        costs (for [source (cities-by-player player)]
                (let [vcp (dijkstra vertexes edges source)
                      [cost _] (vcp city)]
                  cost))]
    (apply min costs)))

(defn city-connection-cost
  "Returns the least expensive cost for the current player to connect to
  a city, including the base city cost and some number of 'edge'
  connection costs. Returns nil if there is no valid connection."
  [{:keys [cities-by-player player step] :as state} city]
  (if-not (candidate-city? state city)
    nil
    (let [cities (cities-by-player player)
          base-cost (base-city-connection-cost step)]
      (if (empty? cities)
        base-cost
        (+ base-cost (city-interconnect-cost state city))))))

(defn cost-by-city
  "Returns a map of [city cost] pairs."
  [state]
  (->> (candidate-cities state)
       (map (fn [c] [c (city-connection-cost state c)]))
       (into {})))
