(ns pg.res-supply.standard)

(def initial-resources
  {:coal    24
   :oil     18
   :garbage  6
   :uranium  2})

(def max-resources
  "The game has a maximum supply of resources. Both resources in the
  market and resources kept in player inventory count against this
  maximum. (The latter is a bit unrealistic, in my opinion.)"
  {:coal    24
   :oil     24
   :garbage 24
   :uranium 12})

(def resource-supply
  "A map where each key is the number of players and each value is a
  map. Inside that map, each key is the step number and the value is a
  map of resource supply replenishment per turn."
  {2 {1 {:coal 3 :oil 2 :garbage 1 :uranium 1}
      2 {:coal 4 :oil 2 :garbage 2 :uranium 1}
      3 {:coal 3 :oil 4 :garbage 3 :uranium 1}}
   3 {1 {:coal 4 :oil 2 :garbage 1 :uranium 1}
      2 {:coal 5 :oil 3 :garbage 2 :uranium 1}
      3 {:coal 3 :oil 4 :garbage 3 :uranium 1}}
   4 {1 {:coal 5 :oil 3 :garbage 2 :uranium 1}
      2 {:coal 6 :oil 4 :garbage 3 :uranium 2}
      3 {:coal 4 :oil 5 :garbage 4 :uranium 2}}
   5 {1 {:coal 5 :oil 4 :garbage 3 :uranium 2}
      2 {:coal 7 :oil 5 :garbage 3 :uranium 3}
      3 {:coal 5 :oil 6 :garbage 5 :uranium 2}}
   6 {1 {:coal 7 :oil 5 :garbage 3 :uranium 2}
      2 {:coal 9 :oil 6 :garbage 5 :uranium 3}
      3 {:coal 6 :oil 7 :garbage 6 :uranium 3}}})
