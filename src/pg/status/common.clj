(ns pg.status.common
  (:require
   [clojure.string :as str]))

(defn format-money
  [x]
  (if x
    (str x "E")
    "N/A"))

(defn player-money
  [{:keys [money-by-player] :as state} player]
  (format-money (money-by-player player)))

(defn plant-summary
  [{:keys [plants] :as state} plant-id]
  (let [[req out] (plants plant-id)]
    (format "#%s:%s->%s" plant-id req out)))

(defn plants-summary
  [state plant-ids]
  (if-not plant-ids
    "N/A"
    (if (empty? plant-ids)
      "None"
      (->> plant-ids
           sort
           (map #(plant-summary state %))
           (str/join " ")))))
