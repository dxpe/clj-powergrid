(ns pg.status.short
  (:require
   [clojure.string :as str]
   [pg.phase :as phase]
   [pg.player :as player]
   [pg.status.common :as c]))

(defn- player-summaries
  "Returns a seq of strings summarizing each player."
  [{:keys [cities-by-player plants-by-player player-order] :as state}]
  (->> (for [p player-order]
         (let [city-count (player/city-count state)
               plants (plants-by-player p)
               max-plant (if (seq plants) (apply max plants) "N/A")]
           (format "%11s %11s %2d %s\n"
                   "" p city-count max-plant)))))

(defn order-players
  [{:keys [cities-by-player plants-by-player player-order turn] :as state}]
  {:pre [(map? state)]}
  (let [s (format "%11s Player Order : %s"
                  "" player-order)]
    (if (> turn 1)
      (str/join "\n" [s] (player-summaries state))
      s)))

(defn auction-plants
  [{:keys [auction-plant auction-players future-market
           highest-bid highest-bidder market next-players]
    :as state}]
  {:pre [(map? state)]}
  (if auction-plant
    (format (str "%11s Auction Players : %s\n"
                 "%11s Next Players    : %s\n"
                 "%11s Auction Plant   : %s\n"
                 "%11s Highest Bid     : %s\n"
                 "%11s Highest Bidder  : %s")
            "" auction-players
            "" next-players
            "" auction-plant
            "" highest-bid
            "" highest-bidder)
    (format (str "%11s Auction Players : %s\n"
                 "%11s Market          : %s\n"
                 "%11s Future Market   : %s")
            "" auction-players
            "" market
            "" future-market)))

(defn buy-resources
  [{:keys [money-by-player plants plants-by-player player
           resources resources-by-player]
    :as state}]
  {:pre [(map? state)]}
  (let [player-plants (plants-by-player player)]
    (format (str "%11s Money            : %s\n"
                 "%11s Plant Details    : %s\n"
                 "%11s Player Resources : %s\n"
                 "%11s Game Resources   : %s")
            "" (c/player-money state player)
            "" (c/plants-summary state player-plants)
            "" (or (resources-by-player player) "N/A")
            "" (or resources "N/A"))))

(defn build-network
  [{:keys [] :as state}]
  {:pre [(map? state)]}
  nil)

(defn power-network
  [{:keys [] :as state}]
  {:pre [(map? state)]}
  nil)

(defn add-resources
  [{:keys [] :as state}]
  {:pre [(map? state)]}
  nil)

(defn adjust-market
  [{:keys [] :as state}]
  {:pre [(map? state)]}
  nil)

(defn status
  [{:keys [phase player step turn] :as state}]
  {:pre [(map? state)]}
  (let [complete? (phase/phase-complete? state)
        phase' (if complete? (str phase " (complete)") phase)
        base (format "%-11s %-31s Turn %s  Step %s"
                     (player/format player)
                     (or phase' "N/A")
                     (or turn "N/A")
                     (or step "N/A"))]
    (if complete?
      base
      (let [more (case phase
                   :order-players  (order-players state)
                   :auction-plants (auction-plants state)
                   :buy-resources  (buy-resources state)
                   :build-network  (build-network state)
                   :power-network  (power-network state)
                   :add-resources  (add-resources state)
                   :adjust-market  (adjust-market state))]
        (if more (str base "\n" more) base)))))
