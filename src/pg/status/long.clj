(ns pg.status.long
  (:require
   [clojure.string :as str]
   [pg.city :as city]
   [pg.constants.resource]
   [pg.constants.step]
   [pg.phase :as phase]
   [pg.player :as player]
   [pg.resource :as resource]
   [pg.status.common :as c]
   [pg.utility :refer :all]))

(defn status
  [{:keys [auction-plant auction-players cities cities-by-player
           finishing-order future-market highest-bid highest-bidder market
           money-by-player next-players phase plants plants-by-player player
           player-order players resource-supply resources resources-by-player
           revenues step turn] :as state}]
  {:pre [(map? state)]}
  (let [s (atom [])
        p #(swap! s conj %)
        complete? (phase/phase-complete? state)
        phase' (if complete? (str phase " (complete)") phase)
        n-players (count players)
        player-plants (plants-by-player player)
        player-resources (resources-by-player player)]
    (cond
     (not turn)
     (do
       (p (heading (str "Game Status (unstarted)")))
       (p (format "- Players         : %d %s" n-players players))
       (p (format "- Cities          : %d %s" (count cities) cities))
       (p (format "- Revenue Chart   : %s" (sort-by first revenues)))
       (p (format "- Resource Supply"))
       (let [res-sup (resource-supply n-players)]
         (doseq [step pg.constants.step/steps]
           (p (format "  - Step %d        : %s" step (res-sup step))))))

     finishing-order
     (do
       (p (heading "Game Status (finished)"))
       (p (format "- Step            : %d" step))
       (p (format "- Turn            : %s" turn))
       (p (format "- Winner          : %s" (first finishing-order)))
       (p (format "- Game Resources  : %s" resources))
       (p (format "- Market          : %s" market))
       (p (format "- Future Market   : %s" future-market)))

     :else
     (do
       (p (heading "Game Status (in progress)"))
       (p (format "- Step            : %d" step))
       (p (format "- Turn            : %s" (or turn "N/A")))
       (p (format "- Phase           : %s" (or phase' "N/A")))
       (p (format "- Player Order    : %s" (or player-order "N/A")))
       (when next-players
         (p (format "- Next Players    : %s" next-players))
         (p (format "- Player          : %s" (player/format player)))
         (p (format "  - Money         : %s" (c/player-money state player)))
         (p (format "  - Plants        : %s" (or player-plants "N/A")))
         (p (format "    - Details     : %s" (c/plants-summary
                                              state player-plants)))
         (p (format "  - Resources     : %s" (or player-resources "N/A"))))
       (when (= phase :order-players)
         nil)
       (when (= phase :auction-plants)
         (p (format "+ Auction Players : %s" auction-players))
         (p (format "+ Auction Plant   : %s" (or auction-plant "none")))
         (p (format "+ Highest Bid     : %s" (or highest-bid "N/A")))
         (p (format "+ Highest Bidder  : %s" (or highest-bidder "N/A"))))
       (when (= phase :buy-resources)
         nil)
       (when (= phase :build-network)
         (p (format "  + Cities        : %s" (cities-by-player player)))
         (p (format "  + City Options  : %s" (city/cost-by-city state))))
       (when (= phase :power-network)
         nil)
       (when (= phase :add-resources)
         nil)
       (when (= phase :adjust-market)
         nil)
       (p (format "- Game Resources  : %s" resources))
       (p (format "- Market          : %s" market))
       (p (format "- Future Market   : %s" future-market))))
    (str/join "\n" @s)))

(defn player-info
  [{:keys [cities-by-player money-by-player plants plants-by-player
           player-order powered-by-player resources-by-player] :as state}]
  {:pre [(map? state)]}
  (let [s (atom [])
        p #(swap! s conj %)]
    (p (heading "Player Info"))
    (doseq [player player-order]
      (let [player-plants (plants-by-player player)
            cities (cities-by-player player)]
        (p (format "- %s" player))
        (p (format "  - Cities    : %s %s" (count cities) cities))
        (if powered-by-player
          (p (format "    - Powered : %s" (powered-by-player player))))
        (p (format "  - Money     : %s" (c/player-money state player)))
        (p (format "  - Plants    : %s" player-plants))
        (p (format "    - Details : %s" (c/plants-summary state player-plants)))
        (p (format "  - Resources : %s" (resources-by-player player)))))
    (str/join "\n" @s)))

(defn resources
  [{:keys [resource-costs resources] :as state}]
  {:pre [(map? state)]}
  (let [s (atom [])
        p #(swap! s conj %)]
    (p (heading "Resources"))
    (doseq [res pg.constants.resource/resources]
      (let [qty (resources res)
            cost (get-in resource-costs [res qty])]
        (p (format "- %2d %-9s : %2dE" qty (name res) cost))))
    (str/join "\n" @s)))
