(ns pg.phases.adjust-market
  "Phase 5C: Adjust Market"
  (:require
   [pg.deck :as deck]
   [pg.market :as market]
   [pg.phase :as phase]
   [pg.utility :refer :all]))

;; ===== life cycle =====

(defn adjust-market
  "Adjust the power plant market. Called during the :adjust-market
  phase (called bureaucracy in the printed game instructions)."
  [{:keys [step] :as state}]
  (cond
   (<= 1 step 2)
   (market/move-largest-plant-to-deck state)

   (= step 3)
   (market/remove-smallest-plant-and-redraw state)))

(defn prepare
  [state]
  (-> state
      (phase/init-next-players-state nil)
      adjust-market))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [state]
  state)

(defn finish!
  [state-atom]
  (swap! state-atom finish)
  nil)

;; ===== choices =====

(defn choices
  "Returns a sequence of choices."
  [state]
  [])

;; ===== commands =====

(defn effect
  "Just a placeholder; there are no player commands."
  [state command]
  (invalid-cmd!
   "Command issued in add-resources phase."
   "There are no player commands in this phase."))
