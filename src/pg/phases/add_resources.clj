(ns pg.phases.add-resources
  "Phase 5B: Add (Resupply) Resources"
  (:require
   [pg.phase :as phase]
   [pg.resource :as resource]
   [pg.utility :refer :all]))

;; ===== various =====

(defn add-resources
  "Add resources to the game. Called during the :add-resources
  phase (part if the 'resupply resources' part of bureaucracy in the
  printed game instructions). Note that the game has a fixed set of
  resources. Below, I use the `ground-res` to indicate how many
  resources are 'in the ground'. Only resources in the ground are
  eligible for resupply."
  [state]
  (let [res (resource/resources-to-resupply state)]
    (print-msg "Resuppling resources with %s." res)
    (update state :resources #(resource/add-resource-maps res %))))

;; ===== life cycle =====

(defn prepare
  [state]
  (-> state
      (phase/init-next-players-state nil)
      add-resources))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [state]
  state)

(defn finish!
  [state-atom]
  (swap! state-atom finish)
  nil)

;; ===== choices =====

(defn choices
  "Returns a sequence of choices."
  [state]
  [])

;; ===== commands =====

(defn effect
  "Just a placeholder; there are no player commands."
  [state command]
  (invalid-cmd!
   "Command issued in add-resources phase."
   "There are no player commands in this phase."))
