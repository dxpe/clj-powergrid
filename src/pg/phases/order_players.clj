(ns pg.phases.order-players
  "Pbase 1: Order Players"
  (:require
   [pg.phase :as phase]
   [pg.random :as rand]
   [pg.step :as step]
   [pg.utility :refer :all]))

;; ===== various =====

(defn shuffle-players
  [rng players]
  (->> players (rand/shuffle rng) vec))

(defn sort-players
  "Returns a vector of player order, sorting left-to-right according
  to (first) players with more cities and (second, to break ties)
  players with higher-numbered power plants."
  [players cities-by-player plants-by-player]
  (->> (for [p players]
         (let [num-cities (count (p cities-by-player))
               max-plant (max-or-nil (p plants-by-player))]
           (if (and num-cities max-plant)
             [[num-cities max-plant] p]
             (throw (ex-info "Unable to sort players" {})))))
       sort reverse (map second) vec))

(defn player-order
  "Returns a vector of players in some order. On the first turn only,
  randomizes the order; otherwise, calls the sort-players function."
  [{:keys [cities-by-player plants-by-player players rng turn] :as state}]
  (cond
   (= turn 1) (shuffle-players rng players)
   (> turn 1) (sort-players players cities-by-player plants-by-player)
   :else (throw (ex-info "Invalid turn value" {:turn turn}))))

(defn update-player-order
  "Returns an updated state map with :players in order."
  [state]
  (assoc state :player-order (player-order state)))

;; ===== life cycle =====

(defn prepare
  [{:keys [start-step-3?] :as state}]
  (let [state' (-> state
                   update-player-order
                   (phase/init-next-players-state nil))]
    (if start-step-3?
      (step/start-step-3 state') ;; :adjust-market triggered step 3
      state')))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [state]
  state)

(defn finish!
  [state-atom]
  nil)

;; ===== choices =====

(defn choices
  "Returns a sequence of choices."
  [state]
  [])

;; ===== commands =====

(defn effect
  [state command]
  (not-implemented "phases/order-players/effect")
  state)
