(ns pg.phases.auction-plants
  "Phase 2: Auction Plants

  This is the auction phase, perhaps the most complicated phase in the
  game, at least in regards to the logic and state of player interaction
  and turn order. (Other phases have more complicated underlying logic;
  e.g. calculating the least expensive path, but the turn order is very
  predictable.) That said, in this phase, and all other phases, the next
  player is always kept in :player and the next players (starting with
  the immediate next player) is kept in :next-players.

  As mentioned above, in the other phases, the notion of player order is
  simpler. Here, the auction phase must distiguish between the current
  auction and the overall auction phase. Here, :next-players drives the
  turn order. That value tracks the players bidding on the plant
  currently at auction, :auction-plant. In addition, we keep track of
  players, using :auction-players, who are still participating in the
  overall auction phase, even if those players are not participating in
  the current auction. The current highest bid and bidder are kept in
  :highest-bid and :highest-bidder.

  The winner of an auction is not necessarily the current player. In
  many cases, an auction is 'won' when another player passes. If there
  is only one player remaining, he/she can win the bid and will be the
  current player.

  Next, I will explain two optional special cases. After a player wins
  an auction, they may have to scrap a previous plant if they have too
  many. After a plant is scrapped, the game checks to see if a player
  has places to store their resources. If not, the game stops and asks
  the current player which resources to discard.

  These two special cases (plant scrapping and resource discarding)
  interrupt the normal auction cycle. These are enabled and tracked by
  setting :scrap-plant? and/or :discard-resources? to true. For each,
  the game waits on the winning player's decision on what to do; this
  interrupts the normal auction cycle. Internally, the game updates
  the :next-players vector to keep track of which player is next.

  Internal note: the code was more complex and buggier before I made
  sure to consistenly rely on `transfer-won-plant-effect`."
  (:require
   [clojure.string :as str]
   [pg.market :as market]
   [pg.phase :as phase]
   [pg.plant :as plant]
   [pg.resource :as resource]
   [pg.utility :refer :all]))

;; ===== life cycle =====

(defn prepare
  [state]
  (let [state' (phase/init-next-players-state state :>)]
    (merge state' {:auction-players (:next-players state')
                   :auction-plant nil
                   :highest-bid nil
                   :highest-bidder nil
                   :bought-plants nil
                   :scrap-plant? false
                   :discard-resources? false})))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [{:keys [bought-plants] :as state}]
  (let [state' (if (empty? bought-plants)
                 (do
                   (print-msg "Discarding the smallest plant.")
                   (market/remove-smallest-plant-and-redraw state))
                 state)]
    (merge state' {:auction-players nil
                   :auction-plant nil
                   :highest-bid nil
                   :highest-bidder nil
                   :bought-plants nil
                   :scrap-plant? false
                   :discard-resources? false})))

(defn finish!
  [state-atom]
  (swap! state-atom finish)
  nil)

;; ===== choices =====

(defn auction-starting-choices
  "Returns a sequence of choices when it is a players turn to start an
  auction. A player picks a plant from the actual market and offers a
  price at least equal to the face value of the plant. (Note: the rules
  specify that a player must buy a plant in turn 1; therefore, I do not
  list [:pass] as an option for 'starting' choices."
  [money market turn]
  (let [choices (for [plant market
                      x (range plant (inc money))]
                  [:bid x :on plant])]
    (if (= turn 1)
      choices
      (conj choices [:pass]))))

(defn auction-response-choices
  "Returns a sequence of choices to respond to particular plant being
  bid. This function does not need to know the player nor the actual
  plant being auctioned; it only needs to know how much money the player
  has and the highest bid so far."
  [money highest-bid]
  (-> (for [x (range (inc highest-bid) (inc money))]
        [:bid x])
      (conj [:pass])))

(defn scrap-old-plant-choices
  [plants]
  (for [p plants]
    [:scrap p]))

(defn discard-unstorable-resources-choices
  [{:keys [plants-by-player player resources-by-player] :as state}]
  (let [plants (plants-by-player player)
        options (plant/excess-resources-options state plants)]
    (map (fn [o] [:discard o]) options)))

(defn choices
  "Returns a sequence of choices available to the current player. If
  there is no plant currently at auction, a player may bid any amount
  between the face value of the plant and his money on hand. If there is
  a plant currently being auctioned, the player may pass or raise the
  bid by some amount. (This function assumes it will only be called when
  player is set correctly. It does not verify that the current player is
  able to bid.)"
  [{:keys [auction-plant discard-resources? highest-bid highest-bidder market
           money-by-player next-players plants-by-player player scrap-plant?
           turn] :as state}]
  (let [money (money-by-player player)]
    (cond
     scrap-plant? (scrap-old-plant-choices (plants-by-player player))
     discard-resources? (discard-unstorable-resources-choices state)
     auction-plant (auction-response-choices money highest-bid)
     :else (auction-starting-choices money market turn))))

;; ===== commands =====

(defn transfer-won-plant-effect
  "Transfer the won plant to the player."
  [{:keys [auction-plant auction-players highest-bid highest-bidder]
    :as state}]
  (-> state
      (update-in [:money-by-player highest-bidder] - highest-bid)
      (update-in [:plants-by-player highest-bidder] conj auction-plant)
      (market/remove-plant-from-market auction-plant)
      (market/draw-and-add-to-market)
      (update :bought-plants (comp vec conj) auction-plant)
      (merge {:auction-players (vec (remove #{highest-bidder} auction-players))
              :auction-plant nil
              :highest-bid nil
              :highest-bidder nil})))

(defn discard-resources-for-plant?
  "If the supplied plant is scrapped, must resources be discarded?"
  [{:keys [auction-plant plants-by-player player] :as state} plant-to-scrap]
  (plant/too-many-resources? state (-> (plants-by-player player)
                                       (disj plant-to-scrap)
                                       (conj auction-plant))))

(defn scrap-old-plant-effect
  "Returns updated state after scrapping an old plant."
  [{:keys [plants-by-player player scrap-plant?] :as state} plant-to-scrap]
  (if-not scrap-plant?
    (invalid-cmd!
     "Scrapping a plant is not allowed at this point."
     (str "A player can only scrap a plant if he/she just purchased a "
          "power plant and has too many.")))
  (let [player-plants (plants-by-player player)]
    (if-not (player-plants plant-to-scrap)
      (invalid-cmd!
       "Cannot scrap an unowned plant."
       (str "Choose a plant from " player-plants))))
  (let [discard? (discard-resources-for-plant? state plant-to-scrap)
        state' (-> state
                   (update-in [:plants-by-player player] disj plant-to-scrap)
                   (transfer-won-plant-effect)
                   (assoc :scrap-plant? false))]
    (if discard?
      (do
        (print-msg "%s must discard resources."
                   (str/capitalize (name player)))
        (assoc state' :discard-resources? true))
      (phase/next-players-state state' (:auction-players state')))))

(defn discard-unstorable-resources-effect
  "Returns updated state after discarding unstorable resources."
  [{:keys [auction-plant auction-players discard-resources? plants-by-player
           player resources resources-by-player] :as state} discard-res]
  (if-not discard-resources?
    (invalid-cmd!
     "Discarding resources is not allowed at this point."
     (str "A player can only discard resources if he/she just discarded a "
          "power plant and no longer has the ability to store them.")))
  (let [player-plants (plants-by-player player)
        discard-options (plant/excess-resources-options state player-plants)]
    (if-not (resource/resources-in-inventory? state discard-res)
      (invalid-cmd!
       "Cannot discard unowned resources."
       (str "Discard options are " discard-options)))
    (if-not (some #{discard-res} discard-options)
      (invalid-cmd!
       "Invalid discard choice."
       (str "Discard options are " discard-options)))
    (let [play-res (resources-by-player player)]
      (-> state
          (assoc-in [:resources-by-player player]
                    (resource/subtract-resource-maps play-res discard-res))
          (merge {:resources (resource/add-resource-maps resources discard-res)
                  :discard-resources? false})
          (phase/next-players-state auction-players)))))

(defn auction-winner-effect
  "Effect of winning an auction. Moves to the next player unless the
  winning player has to get rid of a power plant."
  [{:keys [auction-plant highest-bid highest-bidder] :as state}]
  (print-msg "%s wins plant %d and pays %dE."
             (str/capitalize (name highest-bidder)) auction-plant highest-bid)
  (if (plant/at-plant-limit? state highest-bidder)
    (do
      (print-msg "%s must scrap a plant."
                 (str/capitalize (name highest-bidder)))
      (-> state
          (assoc :scrap-plant? true)
          (phase/next-players-state [highest-bidder])))
    (let [state' (transfer-won-plant-effect state)]
      (phase/next-players-state state' (:auction-players state')))))

(defn starting-bid-effect
  "Returns an 'updated' state after processing a command for the current
  player to place a starting bid on a power plant."
  [{:keys [auction-plant auction-players discard-resources? market
           money-by-player next-players player scrap-plant?] :as state}
   bid on plant]
  (if (not= :on on)
    (invalid-cmd!
     (format "Unrecognized bid command: %s." [:bid bid on plant])
     "Use [:bid 6 :on 6] to start or [:bid 11] to raise."))
  (if scrap-plant?
    (invalid-cmd!
     "Bidding is not a valid option now."
     "The player must scrap a plant."))
  (if discard-resources?
    (invalid-cmd!
     "Bidding is not a valid option now."
     "The player must discard resources."))
  (if (not-any? #{player} next-players)
    (invalid-cmd!
     "Player ineligible to bid."
     (str "Wait until next turn. A player can only win one plant per turn. "
          "If a player passes on his opening bid, he/she is out of the "
          "auction for the rest of the turn.")))
  (if auction-plant
    (invalid-cmd!
     "Incorrect bid command (an auction is underway)."
     "Try the other form; e.g. [:bid 12]"))
  (if-not (some #{plant} market)
    (invalid-cmd!
     "Plant is not in the market."
     (format "Bid on a plant in the market: %s." market)))
  (if (< bid plant)
    (invalid-cmd!
     "Bid is below face value of plant."
     (format "Increase the bid to %d or more." plant)))
  (if (> bid (money-by-player player))
    (invalid-cmd!
     "Bid is more than the player can afford."
     (format "Decrease the bid to %d or less." (money-by-player player))))
  (let [players' (rotate-left next-players)
        state' (-> state
                   (merge {:auction-plant plant
                           :highest-bid bid
                           :highest-bidder player})
                   (phase/next-players-state players'))]
    (if (= 1 (count players'))
      (auction-winner-effect state')
      state')))

(defn raise-bid-effect
  "Returns an 'updated' state after processing a command for the current
  player to raise a previous bid on a power plant."
  [{:keys [auction-plant discard-resources? highest-bid highest-bidder
           money-by-player next-players player scrap-plant?] :as state}
   bid]
  (if scrap-plant?
    (invalid-cmd!
     "Bidding is not a valid option now."
     "The player must scrap a plant."))
  (if discard-resources?
    (invalid-cmd!
     "Bidding is not a valid option now."
     "The player must discard resources."))
  (if-not auction-plant
    (invalid-cmd!
     "No plant is being auctioned."
     "Try the other form; e.g. [:bid 7 :on 7]."))
  (if (= player highest-bidder)
    (invalid-cmd!
     "Player is already the highest bidder."
     "Unclear. There may be a bug in the program."))
  (if (<= bid highest-bid)
    (invalid-cmd!
     "Bid is below the current highest bid."
     (format "Increase the bid to %d or more." (inc highest-bid))))
  (if (> bid (money-by-player player))
    (invalid-cmd!
     "Bid is more than the player can afford."
     (format "Decrease the bid to %d or less." (money-by-player player))))
  (-> state
      (merge {:highest-bid bid
              :highest-bidder player})
      (phase/next-players-state (rotate-left next-players))))

(defn bid-effect
  "Returns an 'updated' state after processing a command for the current
  player to bid on a power plant. The 2-argument arity raises a previous
  bid on the power plant currently being auctioned. The 4-argument arity
  places the first bid on a particular power plant."
  ([state bid]
     (raise-bid-effect state bid))
  ([state bid on plant]
     (starting-bid-effect state bid on plant)))

(defn pass-on-plant-effect
  "Pass for the current power plant only."
  [{:keys [next-players] :as state}]
  (let [x (dec (count next-players))]
    (cond
     (= x 1) (auction-winner-effect state)
     (> x 1) (phase/next-players-state state (subvec next-players 1))
     :else (throw (ex-info "Unexpected next-players count"
                           {:next-players next-players})))))

(defn pass-on-round-effect
  "Pass for the remaining part of the round."
  [{:keys [auction-players turn] :as state}]
  (if (= turn 1)
    (invalid-cmd!
     "A player cannot pass for a entire round for turn 1."
     "A player must bid and (eventually) buy a power plant in turn 1."))
  (let [players' (subvec auction-players 1)]
    (-> state
        (assoc :auction-players players')
        (phase/next-players-state players'))))

(defn pass-effect
  "Returns 'updated' state after processing a pass command."
  [{:keys [auction-plant discard-resources? scrap-plant?] :as state}]
  (if scrap-plant?
    (invalid-cmd!
     "Passing is not a valid option now."
     "The player must scrap a plant."))
  (if discard-resources?
    (invalid-cmd!
     "Passing is not a valid option now."
     "The player must discard resources."))
  (if auction-plant
    (pass-on-plant-effect state)
    (pass-on-round-effect state)))

(defn effect
  [state command]
  (let [[verb & args] command]
    (case verb
      :bid (apply bid-effect state args)
      :pass (apply pass-effect state args)
      :scrap (apply scrap-old-plant-effect state args)
      :discard (apply discard-unstorable-resources-effect state args))))
