(ns pg.phases.build-network
  "Phase 4: Build Network"
  (:require
   [pg.city :as city]
   [pg.end :as end]
   [pg.market :as market]
   [pg.phase :as phase]
   [pg.utility :refer :all]))

;; ===== life cycle =====

(defn prepare
  [state]
  (phase/init-next-players-state state :<))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [state]
  state)

(defn finish!
  [state-atom]
  (swap! state-atom finish)
  nil)

;; ===== choices =====

(comment
  "As a matter of terminology, when I say 'build a city', I really mean 'connect
  a city'. In Power Grid, cities already exist at the start of the game. Players
  connect cities to their network."

  "If a player has not built his/her first city, he/she can choose any city."

  "If a player already has built a city, afterwards, he/she must choose cities
  that are connected to existing cities."

  "To connect to a city, a player must pay the connection costs between
  cities (each leg is between 0 and 27 Elektro for the USA grid) and the
  connection cost to the city itself (e.g. 10 Elektro for the first, 15 Elektro
  for the second, and 20 Elektro for the third)."

  "The game initialization excludes regions that are out of play, so the
  code here does not have to concern itself with checking regions."

  "Relevant vars include:
  city/city-connection-cost : a map of [step cost] pairs
  city/max-city-connections : a map of [step max-connections] pairs"

  "Relevant portions of game state include:
  :cities              : set of cities
  :connection-costs    : map of [two-city-set cost] pairs
  :step                : current step (1, 2, or 3)
  :player              : current player
  :cities-by-player    : map of {player cities} pairs
  :players-by-city     : map of {city players} pairs
  :money-by-player     : map of {player money} pairs

  This phase is different than the others in that a player may call `act!` more
  than one time. This makes the gameplay easier for humans, I think.")

(defn choices
  "Returns a sequence of choices."
  [{:keys [step player cities-by-player players-by-city
           money-by-player] :as state}]
  (let [budget (money-by-player player)
        cost-by-city (city/cost-by-city state)]
    (->> cost-by-city
         (filter (fn [[city cost]] (<= cost budget)))
         (map first)
         (map (fn [city] [:build city]))
         (cons [:pass]))))

;; ===== commands =====

(defn build-effect
  "Build a city connection. Does not automatically move to the next
  player, since a player may want to build more than one.

  After updating the various keys in state, removes plants from the
  market if they are too small. See also "
  [{:keys [player money-by-player] :as state} city]
  (if-not (city/candidate-city? state city)
    (invalid-cmd!
     "Desired city is not a valid choice."
     "Choose a different city or pass."))
  (let [budget (money-by-player player)
        cost (city/city-connection-cost state city)]
    (if (> cost budget)
      (invalid-cmd!
       "Player cannot afford to connect to the city."
       "Choose a less expensive city or pass."))
    (-> state
        (update-in [:money-by-player player] - cost)
        (update-in [:cities-by-player player] conj city)
        (update-in [:players-by-city city] conj player)
        end/finish-game-if-appropriate
        market/remove-plants-if-too-small)))

(defn pass-effect
  "Returns 'updated' state after processing a pass command."
  [{:keys [next-players] :as state}]
  (phase/next-players-state state (subvec next-players 1)))

(defn effect
  [state command]
  (let [[verb & args] command]
    (case verb
      :build (apply build-effect state args)
      :pass (apply pass-effect state args))))
