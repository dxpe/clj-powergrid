(ns pg.phases.buy-resources
  "Phase 3: Buy Resources"
  (:require
   [pg.market :as market]
   [pg.phase :as phase]
   [pg.plant :as plant]
   [pg.player :as player]
   [pg.resource :as resource]
   [pg.step :as step]
   [pg.utility :refer :all]))

;; ===== life cycle =====

(defn prepare
  [{:keys [start-step-3?] :as state}]
  (let [state' (phase/init-next-players-state state :<)]
    (if start-step-3?
      (-> state' ;; :auction-plants triggered step 3
          market/remove-smallest-plant ;; do not draw replacement
          step/start-step-3)
      state')))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [state]
  state)

(defn finish!
  [state-atom]
  (swap! state-atom finish)
  nil)

;; ===== choices =====

(defn choices
  "Returns a sequence of choices. These are the constraints:

  1. A player can only buy resources available in the market.
  2. Resources are priced according to the market.
  3. A player is limited by his/her available money.
  4. A player is limited by how many resources his/her plants can store.

  Additional notes:

  5. A player has a current inventory.
  6. Passing is always an option.

  Game design notes:

  A. All resources must be bought with one command. This may be less
  convenient for a human player, but listing all the options up-front is
  easier for computer-controlled players.

  B. A player cannot discard resources during this step. (That said,
  this may not make much of a practical difference; in most cases, this
  is not something most players would care to do. However, if a player
  owned a hybrid power plant (using either coal or oil), discarding
  certain resources potentially could help. For example, a player could
  to get rid of coal in order to buy additional oil.)

  Implementation notes:

  Returning a list of valid choices can be viewed as a logic problem
  where constraints must be satisfied. The implementation below does not
  use logic programming, however. Instead, it start with the 'narrowest'
  set of possibilities and filters out the results that don't match the
  other constraints."
  [{:keys [money-by-player plants-by-player player resources
           resources-by-player] :as state}]
  (let [rs (resources-by-player player)
        budget (money-by-player player)]
    (->> (player/player-resource-unused-storage-options state)
         resource/resource-map-options-subsets
         (resource/available-resource-maps state)
         (resource/resource-maps-for-cost state budget)
         (map (fn [x] [:buy x]))
         (cons [:pass]))))

;; ===== commands =====

(defn buy-effect
  "Returns 'updated' state after processing a buy command. Automatically
  moves to the next player."
  [{:keys [money-by-player next-players player resources] :as state}
   resource-map]
  (if-not (resource/available-resource-map? state resource-map)
    (invalid-cmd!
     "Desired resources are not available."
     "Purchase available resources or pass."))
  (let [budget (money-by-player player)]
    (if (> (resource/resource-map-cost state resource-map) budget)
      (invalid-cmd!
       "Player cannot afford desired resources."
       "Purchase less expensive resources or pass.")))
  (if-not (player/storable-resource-map? state resource-map)
    (invalid-cmd!
     "Player's plants cannot store the desired resources."
     "Purchase fewer resources or pass."))
  (let [resources' (resource/subtract-resource-maps resources resource-map)
        cost (resource/resource-map-cost state resource-map)]
    (-> state
        (assoc :resources resources')
        (update-in [:money-by-player player] - cost)
        (update-in [:resources-by-player player]
                   resource/add-resource-maps resource-map)
        (phase/next-players-state (subvec next-players 1)))))

(defn pass-effect
  "Returns 'updated' state after processing a pass command."
  [{:keys [next-players] :as state}]
  (phase/next-players-state state (subvec next-players 1)))

(defn effect
  [state command]
  (let [[verb & args] command]
    (case verb
      :buy (apply buy-effect state args)
      :pass (apply pass-effect state args))))
