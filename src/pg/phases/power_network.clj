(ns pg.phases.power-network
  "Phase 5A: Power Network"
  (:require
   [clojure.math.combinatorics :as combo]
   [clojure.set :as set]
   [clojure.string :as str]
   [pg.phase :as phase]
   [pg.plant :as plant]
   [pg.player :as player]
   [pg.resource :as resource]
   [pg.step :as step]
   [pg.utility :refer :all]))

;; ===== life cycle =====

(defn prepare
  [{:keys [start-step-3?] :as state}]
  (let [state' (phase/init-next-players-state state :>)]
    (cond
     (step/step-2-triggered? state')
     (step/start-step-2 state')

     start-step-3? ;; :build-network triggered step 3
     (step/start-step-3 state')

     :else
     state')))

(defn prepare!
  [state-atom]
  (swap! state-atom prepare)
  nil)

(defn finish
  [state]
  state)

(defn finish!
  [state-atom]
  (swap! state-atom finish)
  nil)

;; ===== choices =====

(defn option-cost
  "Returns the cost of an option. An option is defined as a vector of
  even length with [plant-id resource-map] pairs. Returns nil if the
  option cannot be purchased (i.e. there are not enough game resources
  available)."
  [state option]
  (let [costs (->> option
                   (partition 2)
                   (map (fn [[_ res-map]]
                          (resource/resource-map-cost state res-map))))]
    (if (every? identity costs)
      (reduce + costs))))

(defn option-resource-map
  "Returns the total resources required for an option. An option is
  defined as a vector of even length with [plant-id resource-map]
  pairs."
  [state option]
  {:post [(map? %)]}
  (->> option
       (partition 2)
       (map second)
       (apply resource/add-resource-maps)))

(defn option-resources-available?
  "Does the player have enough resources to carry out the option? An
  option is defined as a vector of [plant-id resource-map] pairs."
  [{:keys [player resources-by-player] :as state} option]
  (resource/resources-in-inventory? state (option-resource-map state option)))

(defn choices
  "Returns a sequence of choices.

  Internal note: it was significantly easier to temporarily assoc in the
  plant number to the resource map (using the :id key). (Another way I
  tried, namely using a vector with the plant number followed by the
  resource map, made it painfully complicated to work with the cartesian
  product."
  [{:keys [plants-by-player player resources-by-player] :as state}]
  (let [player-plants (plants-by-player player)]
    (->> player-plants
         (map (fn [p] (->> (plant/plant-resource-requirement-options state p)
                           (map #(assoc % :id p)))))
         combo/subsets
         (mapcat #(apply combo/cartesian-product %))
         ;; Now, a list of options, each a map.
         (map #(mapcat (fn [e] [(:id e) (dissoc e :id)]) %))
         ;; Now, a list of options, each a vector of some length, e.g:
         ;; [5 {:oil 2}] or [5 {:coal 2} 13 {}].
         (filter #(option-resources-available? state %))
         ;; Now, all options have been vetted.
         (map #(vec (cons :power (vec %)))))))

;; ===== commands =====

(defn all-player-plants
  [{:keys [plants-by-player player] :as state}]
  {:post [(set %)]}
  (let [player-plants (plants-by-player player)]
    (if (empty? player-plants)
      (invalid-cmd!
       "Player has no plants."
       "Use [:power :none].")
      player-plants)))

(defn plants<-args
  "Returns the plants corresponding to args."
  [state args]
  {:post [(set? %)]}
  (cond
   (or (nil? args) (= args (list :none)))
   #{}

   (= args (list :all))
   (all-player-plants state)

   (seq? args)
   (let [plants (set (filter number? args))
         all-plants (all-player-plants state)]
     (if (= plants (set/intersection all-plants plants))
       plants
       (invalid-cmd!
        "Player does not own specified plants."
        (format "Choose plants(s) from %s." all-plants))))

   :else
   (invalid-cmd!
    (str "Unexpected args: " args)
    "Valid examples include [:power 6] and [:power 5 {:oil 2}].")))

(defn plant-resource-req
  "Returns the resource requirement for the given plant."
  [state plant]
  {:post [(map? %)]}
  (let [options (plant/plant-resource-requirement-options state plant)
        k (count options)]
    (cond
     (= k 1) (first options)
     (= k 0) {} ;; renewable plant
     (> k 1)
     (invalid-cmd!
      (str "Resources for plant " plant " are ambiguous.")
      "Specify resources after plant; e.g. [:power 5 {:oil 2}]"))))

(defn valid-resource-map?
  "Is the specified resource map valid for the plant?"
  [state plant resource-map]
  (let [options (plant/plant-resource-requirement-options state plant)]
    ((set options) resource-map)))

(defn resources<-pair
  "Parses arguments after :power two at a time, offset by 1, so that
  nothing gets skipped. Two at a time makes sure that a command
  like [:power 5 {:oil 2}] can be parsed.

  The `cond` inside handles the various possibilities. Here are examples
  showing the possibilities:

  1. 5 nil : Try to use resources for plant 5.

  2. 5 6 : Try to use resources for plant 5. Plant 6 doesn't matter,
  since it will be processed on the next cycle.

  2. 5 {:oil 2} : Use {:oil 2}, provided that it matches the valid
  options that can power plant 5.

  3. {:oil 2} nil : Disregard, since this is presumably at the end of a
  list. (Note: the parser could probably be improved to catch this as an
  error if no other input is provided.)

  4. {:oil 2} 12 : Disregard, since 12 will get processed in the next
  cycle."
  [state a b]
  (cond
   (and (number? a) (or (nil? b) (number? b)))
   (plant-resource-req state a)

   (and (number? a) (map? b))
   (if (valid-resource-map? state a b)
     b
     (invalid-cmd!
      "Specified resources do not match plant."
      (format "The resource options for plant %d are: %s."
              a (plant/plant-resource-requirement-options state a))))

   (map? a)
   {}

   :else
   (invalid-cmd! "Unexpected args." "Example: [:power 5 {:coal 2}]")))

(defn resources<-args
  "Returns a resource map corresponding to args."
  [{:keys [plants-by-player player] :as state} args]
  {:post [(map? %)]}
  (cond
   (or (nil? args) (= args (list :none)))
   {}

   (= args (list :all))
   (->> (all-player-plants state)
        (map #(plant-resource-req state %))
        (apply resource/add-resource-maps))

   (seq? args)
   (->> (partition-all 2 1 args)
        (map (fn [[a b]] (resources<-pair state a b)))
        (apply resource/add-resource-maps))

   :else
   (invalid-cmd!
    (str "Unexpected args: " args)
    "Valid examples include [:power 6] and [:power 5 {:oil 2}].")))

(defn power-capacity
  "Returns the number of cities the chosen plants can power."
  [{:keys [plants] :as state} chosen-plants]
  (->> chosen-plants (map plants) (map second) (reduce +)))

(defn income-from-powered-cities
  [{:keys [revenues] :as state} pow-cities]
  (if-let [x (revenues pow-cities)]
    x
    (invalid-cmd!
     (format "Could not find revenue for %d cities." pow-cities)
     "This is a bug; please report it to the game developer.")))

(defn print-result
  [player pow-cities req-res income]
  (let [noun (if (= 1 pow-cities) "city" "cities")]
    (print-msg
     "%s powers %d %s using %s and earns %dE."
     (str/capitalize (name player)) pow-cities noun req-res income)))

(defn power-effect
  "Power some power plants. Automatically moves the next
  player. Internal note: see also `pg.end/use-resources`."
  [{:keys [next-players player resources-by-player] :as state} args]
  (let [chosen-plants (plants<-args state args)
        req-res (resources<-args state args)
        player-res (resources-by-player player)
        player-res' (resource/subtract-resource-maps player-res req-res)]
    (if-not (resource/valid-resource-map? player-res')
      (invalid-cmd!
       "Player does not have enough resources."
       "Power different and/or fewer plants.")
      (let [pow-cap (power-capacity state chosen-plants)
            num-cities (player/city-count state)
            pow-cities (min pow-cap num-cities)
            income (income-from-powered-cities state pow-cities)]
        (print-result player pow-cities req-res income)
        (-> state
            (update-in [:money-by-player player] + income)
            (assoc-in [:resources-by-player player]
                      (resource/collapse-resource-map player-res'))
            (phase/next-players-state (subvec next-players 1)))))))

(defn effect
  "Power some power plants. Below are the supported commands.

  Power plants 6 and 10:
  [:power 6 10]

  Power plant 5, a hybrid:
  [:power 5 {:coal 1 :oil 1}]

  Power plant 6 and 12 (a hybrid):
  [:power 6 12 {:coal 2}]

  Or:
  [:power 12 {:coal 2} 6]

  Power all plants, if possible:
  [:power :all]
  (If not possible, throws an exception.)

  Power no plants:
  [:power]

  Or:
  [:power :none]"
  [state command]
  (let [[verb & args] command]
    (case verb
      :power (power-effect state args))))
