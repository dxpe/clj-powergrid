(comment
  (defn end-auction-effect
    "Updates and returns state. Ends the auction."
    [{:keys [auction-players highest-bidder] :as state}]
    (let [players' (->> auction-players (remove #{highest-bidder}) vec)]
      (-> state
          (merge {:auction-players players'
                  :auction-plant nil
                  :highest-bid nil
                  :highest-bidder nil})))))
