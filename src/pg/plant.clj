(ns pg.plant
  (:require
   [clojure.math.combinatorics :as combo]
   [pg.resource :as resource]))

(def ^:const max-plants
  "A map where the values are the maximum number of power plants per
  user, and the keys are the number of players."
  {2 4
   3 3
   4 3
   5 3
   6 3})

(defn plant-resource-requirement
  "Returns a 'raw' resource requirement to run a plant. For example,
  plant #3 returns [2 :oil]. Plant #5 returns [2 :hybrid]. Plant #18
  returns [] because it is renewable and requires no burnable
  resources."
  [{:keys [plants] :as state} plant]
  {:pre [(map? state) (number? plant)] :post [(vector? %)]}
  (if-let [p (plants plant)]
    (first p)
    (throw (ex-info (format "Cannot find power plant %d." plant) {}))))

(defn plant-resource-requirement-options
  "Returns a vector of possible resource requirements to run a
  plant. Each resource requirement is a map. For example, plant
  #3, [2 :oil], returns [{:oil 2}]. As a more complicated example, plant
  #5, [2 :hybrid], returns [{:oil 2} {:coal 2} {:oil 1 :coal 1}]. Plant
  #18, a renewable power plant, returns []."
  [{:keys [plants] :as state} plant]
  {:pre [(map? state) (number? plant)] :post [(vector? %)]}
  (let [res-req (plant-resource-requirement state plant)]
    (if (empty? res-req)
      []
      (let [[quantity resource] res-req]
        (resource/resource-options resource quantity)))))

(defn plant-resource-requirement-options*
  "Behaves the same as plant-resource-requirement-options except for one
  case; this will return [{}] instead of []."
  [state plant]
  {:pre [(map? state) (number? plant)] :post [(vector? %)]}
  (let [opts (plant-resource-requirement-options state plant)]
    (if (empty? opts)
      [{}]
      opts)))

(defn plant-resource-storage-options
  "Returns a vector of maximum storage possibilities for a plant. Each
  resource possibility is a map. For example, plant #3, [2 :oil],
  returns [{:oil 4}]. As a more complicated example, plant
  #5, [2 :hybrid], returns [{:coal 4} {:coal 3 :oil 1} {:coal 2 :oil 2}
  {:coal 1 :oil 3} {:oil 4}]."
  [state plant]
  {:pre [(map? state) (number? plant)] :post [(vector? %)]}
  (let [res-req (plant-resource-requirement state plant)]
    (if (empty? res-req)
      []
      (let [[quantity resource] res-req]
        (resource/resource-options resource (* 2 quantity))))))

(defn plant-resource-storage-options*
  "Behaves the same as plant-resource-storage-options except for one
  case; this will return [{}] instead of []."
  [state plant]
  {:pre [(map? state) (number? plant)] :post [(vector? %)]}
  (let [opts (plant-resource-storage-options state plant)]
    (if (empty? opts)
      [{}]
      opts)))

(defn plants-resource-storage-options
  "Returns a vector of maximum storage possibilities for selected
  plants. Each resource possibility is a map."
  [state selected-plants]
  {:pre [(map? state)] :post [(vector? %)]}
  (if (empty? selected-plants)
    []
    (->> selected-plants
         (map #(plant-resource-storage-options* state %))
         (apply combo/cartesian-product)
         (map #(apply resource/add-resource-maps %))
         (remove empty?)
         distinct
         vec)))

(defn at-plant-limit?
  "Is the specified player at the maximum limit of plants?"
  [{:keys [plants-by-player players] :as state} the-player]
  (let [max-p (max-plants (count players))
        num-p (count (plants-by-player the-player))]
    (= num-p max-p)))

(defn plants-power-capacity
  "Returns the power plant capacity of selected-plants."
  [{:keys [plants] :as state} selected-plants]
  (->> selected-plants
       (map plants)
       (map second)
       (reduce +)))

;; TODO: consider moving to resource.clj
(defn too-many-resources?
  "Does the current player have too many resources for the selected
  plants?"
  [{:keys [player resources-by-player] :as state} selected-plants]
  (let [resources (resources-by-player player)
        options (plants-resource-storage-options state selected-plants)
        valid-option? (fn [option]
                        (-> option
                            (resource/subtract-resource-maps resources)
                            resource/valid-resource-map?))]
    (not (some valid-option? options))))

;; TODO: consider moving to resource.clj
(defn excess-resources-options
  "Returns a sequence of options representing an excess of resources
  over what the selected plants can store. Each returned option is a
  resource map that, if discarded, would get a player back to the
  condition of having a valid amount of resources for the selected
  plants. See also plant/too-many-resources?.

  Only 'minimal' excesses are kept, as defined by total resources;
  e.g. if there are options for a 1 card discard, only those options are
  returned."
  [{:keys [player resources-by-player] :as state} selected-plants]
  (let [player-res (resources-by-player player)
        options (plants-resource-storage-options state selected-plants)]
    (if (empty? options)
      [player-res]
      (let [excesses (map #(resource/excess-of player-res %) options)]
        (if (some empty? excesses)
          []
          (let [res-qty #(reduce + (vals %))
                min-res (->> (remove empty? excesses)
                             (map res-qty)
                             (apply min))]
            (->> excesses
                 (filter #(= min-res (res-qty %)))
                 distinct vec)))))))
