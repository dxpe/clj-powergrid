(ns pg.turn
  (:require
   [pg.constants.phase :refer [phases]]
   [pg.phase :as phase]))

(defn turn-complete?
  [{:keys [phase] :as state}]
  (and (= phase (last phases))
       (phase/phase-complete? state)))
