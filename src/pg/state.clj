(ns pg.state
  (:require
   [clojure.string :as str]
   [pg.city :as city]
   [pg.constants.grid :refer [region-count]]
   [pg.constants.player :refer [initial-money]]
   [pg.constants.step :refer [steps]]
   [pg.deck :as deck]
   [pg.market :as market]
   [pg.random :as rand]
   [pg.utility :refer :all]))

(defn config-errors
  [{:keys [players regions] :as config}]
  (let [errors (atom [])
        err #(swap! errors conj %)]
    (let [a (count players)]
      (if-not (<= 2 (count players) 6)
        (err (format "Expected 2 to 6 players but found %d." a))))
    (let [a (count regions)
          e (region-count (count players))]
      (if-not (= e a)
        (err (format "Expected %d regions but found %d." e a))))
    @errors))

(defn expect
  [key v pred pred-name]
  {:pre [(keyword? key) (fn? pred) (string? pred-name)]}
  (if-not v
    (throw (ex-info (str "Missing value for %s" key) {})))
  (if-not (pred v)
    (throw (ex-info (str "Config " key " must be a " pred-name) {}))))

(defn init-dynamic-state
  "Returns the initial dynamic (e.g. changeable) state. This may seem to
  be a strange use of the word 'changable', because most people would
  thing of state as being changeable (e.g. dynamic). Yes, that is
  generally true, but in the case of this game, there are certain parts
  of the state that are static, such as the grid (the map), the possible
  set of power plants, and the maximum quantity for each resource. These
  things may be configured before the game starts but are not changed
  afterwards. This static state is also kept in the same atom as a
  matter of convenience.

  Shared across all phases:
  :step                : current step (1, 2, or 3)
  :turn                : current turn number (long)
  :player-order        : players in order for current turn (vector)
  :phase               : current phase (keyword) (see note 1)
  :player              : current player (keyword)
  :next-players        : players remaining in current phase (vector)
  :market              : market power plants (vector of plant ids)
  :future-market       : future plant market (vector of plant ids)
  :deck                : plant ids and :step-3 card (vector)
  :cities-by-player    : {player cities} pairs (map)
  :players-by-city     : {city players} pairs (map)
  :money-by-player     : {player money} pairs (map)
  :plants-by-player    : {player plants} pairs (map)
  :resources-by-player : {player resources} pairs (map)
  :resources           : {resource quantity} pairs (map)
  :start-step-3?       : start step 3 in the next phase? (boolean)
  :powered-by-player   : {num-cities-powered player} pairs at finish (map)
  :finishing-order     : winning player then runners-up (vector)

  Used only in the auction phase:
  :auction-players     ; players in the auction phase (set) (see note 2)
  :auction-plant       : current plant being auctioned (keyword)
  :highest-bid         : highest bid on plant for auction (long)
  :highest-bidder      : highest bidder in auction (keyword)
  :bought-plants       : plants bought in this turn
  :scrap-plant?        : current player must scrap a plant? (boolean)
  :discard-resources?  : current player must discard resources? (boolean)

  Notes:
  1. :phase start at `nil`, meaning setup.
  2. :auction-players includes players, for this turn, that have (a) not
     won not won a plant at auction and (b) not passed when it was their
     turn to start a new auction for a plant."
  [{:keys [cities connections cost-by-connection initial-resources
           max-resources plants players regions regions-by-city resource-costs
           resource-supply revenues rng] :as config}]
  (expect :cities cities set? "set")
  (expect :connections connections set? "set")
  (expect :cost-by-connection cost-by-connection map? "map")
  (expect :initial-resources initial-resources map? "map")
  (expect :max-resources max-resources map? "map")
  (expect :plants plants map? "map")
  (expect :players players set? "set")
  (expect :regions regions set? "set")
  (expect :regions-by-city regions-by-city map? "map")
  (expect :resource-costs resource-costs map? "map")
  (expect :resource-supply resource-supply map? "map")
  (expect :revenues revenues map? "map")
  (expect :rng rng #(instance? java.util.Random %) "RNG")
  {:step                (first steps)
   :turn                nil
   :player-order        nil
   :phase               nil
   :player              nil
   :next-players        nil
   :auction-players     nil
   :auction-plant       nil
   :highest-bid         nil
   :highest-bidder      nil
   :bought-plants       nil
   :scrap-plant?        false
   :discard-resources?  false
   :market              (market/initial-market plants)
   :future-market       (market/initial-future-market plants)
   :deck                (deck/initial-deck rng plants (count players))
   :cities-by-player    (zipmap players (repeat #{}))
   :players-by-city     (zipmap cities (repeat #{}))
   :money-by-player     (zipmap players (repeat initial-money))
   :plants-by-player    (zipmap players (repeat #{}))
   :resources-by-player (zipmap players (repeat {}))
   :resources           initial-resources
   :start-step-3?       false
   :powered-by-player   nil
   :finishing-order     nil})

(defn init-state
  "Returns the initial game state when given a `config` argument, a map
  of fixed initial state."
  [config]
  (let [errors (config-errors config)]
    (if (seq errors)
      (throw (ex-info (str "Config errors: " (str/join " " errors))
                      {:errors errors}))
      (safe-merge config (init-dynamic-state config)))))
