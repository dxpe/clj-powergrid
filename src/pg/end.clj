(ns pg.end
  (:require
   [clojure.math.combinatorics :as combo]
   [pg.constants.city :refer [cities-to-trigger-end]]
   [pg.city :as city]
   [pg.plant :as plant]
   [pg.resource :as resource]
   [pg.utility :refer :all]))

(defn resources-to-power-set
  "If the plant-set can be powered, returns a map of {plant-id
  resource-map} pairs that suffice. Otherwise (i.e. if the plant-set
  cannot be powered), returns nil.

  Internal note: I chose to keep some intermediate data in the threading
  chain. There could be better ways to handle this, but it seemed like a
  reasonable choice. I used a similar approach in `choices` in
  `pg.phases.power-network`."
  [state player plant-set]
  {:pre [(set? plant-set)] :post [(or (nil? %) (map %))]}

  (->> plant-set
       (map (fn [p] (->> (plant/plant-resource-requirement-options* state p)
                         (map (fn [res-opt] {p res-opt})))))
       (apply combo/cartesian-product)
       (map #(into {} %))
       ;; each item is a map with {plant res-map} pairs
       (filter #(->> (apply resource/add-resource-maps (vals %))
                     (resource/resources-in-inventory? state player)))
       first))

(defn use-resources
  "Discards the supplied resources from the player. Internal note: see
  also `pg.phases.power-network/power-effect`."
  [{:keys [resources-by-player] :as state} player res-maps]
  (let [res (apply resource/add-resource-maps res-maps)
        player-res (resources-by-player player)
        res' (->> (resource/subtract-resource-maps player-res res)
                  resource/collapse-resource-map)]
    (assoc-in state [:resources-by-player player] res')))

(defn powered-cities
  "Returns the number of cities a set of plants can power, not exceeding
  the number of cities owned by a player."
  [{:keys [cities-by-player plants] :as state} player plant-set]
  {:pre [(set? plant-set)] :post [(number? %)]}
  (min (plant/plants-power-capacity state plant-set)
       (count (cities-by-player player))))

(defn power-set
  "Powers many cities as possible using the given plants, specified
  using the `plant-map` argument, containing {plant-id resource-map}
  pairs. Returns updated state."
  [state player plant-map]
  {:pre [(map? plant-map)]}
  (-> state
      (use-resources player (vals plant-map))
      (assoc-in [:powered-by-player player]
                (powered-cities state player (set (keys plant-map))))))

(defn player-power-max-cities
  "Automatically powers the maximum number of cities for a
  player. Returns updated state."
  [{:keys [cities plants plants-by-player] :as state} player]
  (let [player-plants (plants-by-player player)
        plant-sets (map set (combo/subsets player-plants))
        q<-plant #(let [[_ q] (plants %)] q)
        q<-plants #(reduce + (map q<-plant %))]
    (loop [candidate-sets (sort-by (comp - q<-plants) plant-sets)]
      (if-let [plant-set (first candidate-sets)]
        (if-let [plant-map (resources-to-power-set state player plant-set)]
          (power-set state player plant-map)
          (recur (next candidate-sets)))
        (throw (ex-info (str "Internal error: player " player
                             " cannot power any option.") {}))))))

(defn players-power-max-cities
  "Automatically powers the maximum number of cities for each
  player. Returns updated state."
  [{:keys [players] :as state}]
  (reduce player-power-max-cities state players))

(defn score-player
  "Returns a vector used for sorting players into the final winning
  order. (Note: yes, I could have used `clojure.core/juxt` here.)"
  [{:keys [cities-by-player money-by-player powered-by-player]
    :as state} player]
  (let [num-powered (powered-by-player player)
        money (money-by-player player)
        num-cities (count (cities-by-player player))]
    [num-powered money num-cities]))

(defn set-finishing-order
  "Updates state with finishing order."
  [{:keys [players] :as state}]
  (assoc state :finishing-order
         (->> players (sort-by #(score-player state %)) reverse vec)))

(defn finish-game
  "Finishes the game. Returns updated state with :finishing-order where
  the winner is listed first."
  [state]
  (print-msg "Game ending condition has been triggered.")
  (-> state
      players-power-max-cities
      set-finishing-order))

(defn finish-game-if-appropriate
  "Finishes the game and declares a winner, if appopriate. Returns an
  updated state."
  [{:keys [cities-by-player player players] :as state}]
  (let [num-cities (count (cities-by-player player))
        city-threshold (cities-to-trigger-end (count players))]
    (if (< num-cities city-threshold)
      state
      (do
        (finish-game state)))))
