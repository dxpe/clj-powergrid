(ns pg.market
  "Power plant market."
  (:require
   [pg.constants.market :refer [market-by-step]]
   [pg.deck :as deck]
   [pg.player :as player]
   [pg.utility :refer :all]))

(defn initial-market
  [plants]
  (let [actual (get-in market-by-step [1 :actual])]
    (->> plants keys sort (take actual) vec)))

(defn initial-future-market
  [plants]
  (let [actual (get-in market-by-step [1 :actual])
        future (get-in market-by-step [1 :future])]
    (->> plants keys sort (drop actual) (take future) vec)))

(defn split-market
  "Splits market into actual and future portions according to current
  step. Updates state (:market, :future-market)."
  [{:keys [future-market market step] :as state} mkt]
  (let [{:keys [actual future]} (market-by-step step)]
    (-> state
        (assoc :market (->> mkt (take actual) vec))
        (assoc :future-market (->> mkt (drop actual) (take future) vec)))))

(defn concat-and-split-market
  "Concat market and future market and split according to step."
  [{:keys [future-market market step] :as state}]
  (split-market state (concat market future-market)))

(defn remove-plant-from-market
  "Remove plant from market or future market."
  [{:keys [future-market market] :as state} plant]
  (->> (concat market future-market)
       (remove #{plant})
       (split-market state)))

(defn remove-smallest-plant
  "Remove the smallest plant from the market."
  [{:keys [market] :as state}]
  (if (empty? market)
    state
    (remove-plant-from-market state (apply min market))))

(defn add-plant-to-market
  "Add a plant to the market."
  [{:keys [future-market market step] :as state} plant]
  {:pre [(map? state) (number? plant)]}
  (->> (concat market future-market)
       (cons plant)
       sort
       (split-market state)))

(defn prepare-for-step-3
  "Prepare for step 3. Returns updated state."
  [{:keys [phase step] :as state}]
  (print-msg "Preparing for step 3.")
  (let [state' (-> state
                   (assoc :start-step-3? true)
                   deck/shuffle-deck)]
    (case phase
      :auction-plants state'
      :build-network (remove-smallest-plant state')
      :adjust-market (remove-smallest-plant state'))))

(defn draw-and-add-to-market
  "Draw a card from the deck. If it is a plant card (most likely), move
  it to the market or future market. If it is step 3, handle it. If the
  deck is exhausted, do nothing. Returns updated state."
  [{:keys [deck] :as state}]
  {:pre [(vector? deck) (map? state)]}
  (let [[card state'] (deck/draw-card state)]
    (cond
     (nil? card) state
     (= card :step-3) (prepare-for-step-3 state')
     :else (add-plant-to-market state' card))))

(defn remove-smallest-plant-and-redraw
  [state]
  (-> state remove-smallest-plant draw-and-add-to-market))

(defn move-largest-plant-to-deck
  [{:keys [future-market] :as state}]
  (let [max-plant (apply max future-market)]
    (-> state
        (remove-plant-from-market max-plant)
        (deck/add-to-deck-bottom max-plant)
        draw-and-add-to-market)))

(defn remove-plants-if
  "Removes plants from the market that match the predicate."
  [{:keys [future-market market] :as state} pred]
  (let [combined-market (concat market future-market)
        plants-to-remove (filter pred combined-market)]
    (if (empty? plants-to-remove)
      state
      (let [plant (first plants-to-remove)]
        (print-msg "Removing plant %d from the market." plant)
        (-> state
            (remove-plant-from-market plant)
            draw-and-add-to-market)))))

(defn remove-plants-if-too-small
  "Removes plants from the market if their face value is less than or
  equal than the maximum number of cities owned by a player. (See also
  `deck/draw-card` which ensures that drawn cards have a high enough
  face value.)"
  [{:keys [future-market market] :as state}]
  (let [n (player/city-count state)]
    (remove-plants-if state #(<= % n))))
