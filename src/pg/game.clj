(ns pg.game
  (:require
   [pg.phase :as phase]
   [pg.constants.phase :refer [phases]]
   [pg.phases.add-resources :as add-resources]
   [pg.phases.adjust-market :as adjust-market]
   [pg.phases.auction-plants :as auction-plants]
   [pg.phases.build-network :as build-network]
   [pg.phases.buy-resources :as buy-resources]
   [pg.phases.order-players :as order-players]
   [pg.phases.power-network :as power-network]
   [pg.player :as player]
   [pg.resource :as resource]
   [pg.status.short :as short-status]
   [pg.status.long :as long-status]
   [pg.turn :as turn]
   [pg.utility :refer :all]))

;; ==== display information ====

(defn short-status
  [state]
  (println (short-status/status state)))

(defn long-status
  [state]
  (println (long-status/status state)))

(defn resources
  [state]
  (println (long-status/resources state)))

(defn player-info
  [state]
  (println (long-status/player-info state)))

;; ==== mutate the game ====

(defn prepare-phase!
  "Prepares a phase. Not intended to be called directly by the
  user. Modifies state-atom."
  [state-atom]
  {:pre [(atom? state-atom)]}
  (let [phase (:phase @state-atom)]
    (case phase
      :order-players  (order-players/prepare! state-atom)
      :auction-plants (auction-plants/prepare! state-atom)
      :buy-resources  (buy-resources/prepare! state-atom)
      :build-network  (build-network/prepare! state-atom)
      :power-network  (power-network/prepare! state-atom)
      :add-resources  (add-resources/prepare! state-atom)
      :adjust-market  (adjust-market/prepare! state-atom)))
  nil)

(defn finish-phase!
  "Prepares a phase. Not intended to be called directly by the user. May
  modify the state-atom."
  [state-atom]
  {:pre [(atom? state-atom)]}
  (let [phase (:phase @state-atom)]
    (case phase
      :order-players  (order-players/finish! state-atom)
      :auction-plants (auction-plants/finish! state-atom)
      :buy-resources  (buy-resources/finish! state-atom)
      :build-network  (build-network/finish! state-atom)
      :power-network  (power-network/finish! state-atom)
      :add-resources  (add-resources/finish! state-atom)
      :adjust-market  (adjust-market/finish! state-atom)))
  nil)

(defn start!
  "Start the game. Modifies the state atom."
  [state-atom]
  {:pre [(atom? state-atom)]}
  (print-msg "Starting Game")
  (swap! state-atom assoc :turn 1)
  (swap! state-atom assoc :phase (first phases))
  (prepare-phase! state-atom)
  (short-status @state-atom))

(defn choices
  "Returns a sequence of choices based on the current phase"
  [{:keys [phase] :as state}]
  {:pre [(map? state)]}
  (if (phase/phase-complete? state)
    []
    (case phase
      :order-players  (order-players/choices state)
      :auction-plants (auction-plants/choices state)
      :buy-resources  (buy-resources/choices state)
      :build-network  (build-network/choices state)
      :power-network  (power-network/choices state)
      :add-resources  (add-resources/choices state)
      :adjust-market  (adjust-market/choices state))))

(defn next-phase!
  "Advances the game to the next phase. Waiting for this function to be
  called gives the player additional control over certain phases in the
  game where the computer automatically updates aspects of the game. In
  allows the user to see what just happened before advancing. For
  example, a player may want to see the new player order before jumping
  into phase 2 (auction power plants). Modifies the state atom."
  [state-atom]
  {:pre [(atom? state-atom)]}
  (let [state @state-atom]
    (if-not (phase/phase-complete? state)
      (throw (ex-info "Current phase not complete." {})))
    (let [current-phase (:phase state)
          next-phase (phase/next-phase current-phase)]
      (finish-phase! state-atom)
      (if next-phase
        (do
          (print-msg "Moving to next phase.")
          (swap! state-atom assoc :phase next-phase))
        (do
          (print-msg "Moving to next turn.")
          (swap! state-atom update :turn inc)
          (swap! state-atom assoc :phase (first phases))))
      (prepare-phase! state-atom)
      (short-status @state-atom))))

(defn effect
  "Returns the effect ('updated' state) of a command. Does not modify
  state atom."
  [{:keys [phase player] :as state} command]
  {:pre [(map? state) (vector? command)]}
  (case phase
    :order-players  (order-players/effect state command)
    :auction-plants (auction-plants/effect state command)
    :buy-resources  (buy-resources/effect state command)
    :build-network  (build-network/effect state command)
    :power-network  (power-network/effect state command)
    :add-resources  (add-resources/effect state command)
    :adjust-market  (adjust-market/effect state command)))

(defn act!
  "Issue command. Modifies the state atom."
  [state-atom command]
  {:pre [(atom? state-atom)]}
  (reset! state-atom (effect @state-atom command))
  (short-status @state-atom))
