(ns pg.deck
  (:require
   [pg.constants.deck :refer [top-of-deck card-removals]]
   [pg.constants.market :refer [market-by-step]]
   [pg.player :as player]
   [pg.random :as rand]
   [pg.utility :refer :all]))

(defn initial-deck
  "Returns an initial deck as a vector."
  [rng plants player-count]
  (let [actual (get-in market-by-step [1 :actual])
        future (get-in market-by-step [1 :future])
        middle (->> plants keys sort
                    (drop (+ actual future))
                    (remove #(= % top-of-deck))
                    (rand/shuffle rng)
                    (drop (card-removals player-count)))]
    (vec (concat [top-of-deck] middle [:step-3]))))

(defn add-to-deck-bottom
  [{:keys [deck] :as state} plant]
  (update state :deck conj plant))

(defn draw-card-until
  "Draws one card at a time until the card is a keyword (e.g. :step-3)
  or pred is true or deck is exhausted. Returns a vector with [card
  update-state]. Note: drawing a card can trigger Step 3; all the
  effects will be stored in the updated state."
  [{:keys [deck] :as state} pred]
  (if-let [card (first deck)]
    (let [state' (update state :deck #(subvec % 1))]
      (if (or (keyword? card) (pred card))
        [card state']
        (recur state' pred)))
    [nil state]))

(defn draw-card
  "Draw one power plant card at a time until the face value is higher
  than the maximum number of cities owned by a player or until the deck
  is exhausted. Returns a vector with [card updated-state]. Note:
  drawing a card can trigger Step 3; all the effects will be stored in
  the updated state. See also `market/remove-plants-if-too-small` which
  updates the market to make sure all plants have a high enough face
  value."
  [{:keys [deck] :as state}]
  (let [n (player/city-count state)]
    (draw-card-until state #(> % n))))

(defn shuffle-deck
  [{:keys [deck rng] :as state}]
  (update state :deck #(vec (rand/shuffle rng %))))
