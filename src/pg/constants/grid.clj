(ns pg.constants.grid)

(def ^:const region-count
  "A map where the values are the number of regions used on the game
  grid (e.g. the map) and the values are the number of players."
  {2 3
   3 3
   4 4
   5 5
   6 5})
