(ns pg.constants.step)

(def ^:const steps
  "A vector of game steps in order."
  [1 2 3])
