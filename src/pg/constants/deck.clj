(ns pg.constants.deck)

(def ^:const top-of-deck
  "The plant number pulled and placed at the top of the deck."
  13)

(def ^:const card-removals
  "A map where the values are the number of randomly removed face-down
  power plants (after preparing the market) and the keys are the number
  of players."
  {2 8
   3 8
   4 4
   5 0
   6 0})
