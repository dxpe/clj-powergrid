(ns pg.constants.phase)

(def ^:const phase-name-pairs
  "The phases as explained in the instructions are:
  1. Determine Player Order
  2. Auction Power Plants
  3. Buy Resources
  4. Building
  5. Bureaucracy

  Below, bureaucracy is split into three parts for convenience:
  * Power Network (earn cash)
  * Add Resources (resupply resources)
  * Adjust Market
    A. move highest plant to bottom of the deck (all steps)
    B. remove lowest plant from the game (step 3 only)"
  [[:order-players  "Determine Player Order"]
   [:auction-plants "Auction Power Plants"]
   [:buy-resources  "Buy Resources"]
   [:build-network  "Build Network"]
   [:power-network  "Power Network"]
   [:add-resources  "Add Resources"]
   [:adjust-market  "Adjust Market"]])

(def ^:const phase-names
  (into {} phase-name-pairs))

(def ^:const phases (map first phase-names))

(def ^:const phase-indexes
  "A map of [phase index] pairs"
  (zipmap phases (range 1 (inc (count phases)))))
