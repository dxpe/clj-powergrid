(ns pg.constants.city)

(def ^:const base-city-connection-cost
  "A map where the values are cost of making a city connection and the keys are
  the step number."
  {1 10
   2 15
   3 20})

(def ^:const max-city-connections
  "A map where the values are the maximum connections for a
  city (e.g. number of players that connect to a particular city) and
  the keys are steps."
  {1 1
   2 2
   3 3})

(def ^:const cities-to-trigger-step-2
  "A map where the values are the number of connected cities to trigger
  step 2 and the keys are the number of players."
  {2 10
   3  7
   4  7
   5  7
   6  6})

(def ^:const cities-to-trigger-end
  "A map where the values are the number of connected cities to trigger
  the game end and the keys are the number of players."
  {2 21
   3 17
   4 17
   5 15
   6 14})
