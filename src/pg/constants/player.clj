(ns pg.constants.player)

(def ^:const initial-money
  "Initial money per player."
  50)
