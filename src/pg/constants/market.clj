(ns pg.constants.market)

(def ^:const market-by-step
  "A map where the keys are step numbers and the values are the number
  of power plants available in the current and future markets."
  {1 {:actual 4 :future 4}
   2 {:actual 4 :future 4}
   3 {:actual 6 :future 0}})
