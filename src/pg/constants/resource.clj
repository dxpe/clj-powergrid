(ns pg.constants.resource)

(def ^:const resources
  "A vector of resources"
  [:coal :oil :garbage :uranium])

(def ^:const empty-resource-map
  (zipmap resources (repeat 0)))
