(ns pg.phase
  (:require
   [pg.constants.phase :refer [phase-indexes phase-name-pairs phases]]
   [pg.market :as market]
   [pg.player :as player]
   [pg.resource :as resource]
   [pg.utility :refer :all]))

(defn phase-complete?
  "Is the current phase complete?"
  [{:keys [next-players] :as state}]
  {:pre [(map? state)]}
  (empty? next-players))

(defn next-phase
  "Returns the next phase. Returns nil when given last phase."
  [phase]
  (if-let [k (phase phase-indexes)]
    (if (< k (count phases))
      (nth phases k))
    (throw (ex-info (str "Cannot find phase " phase) {}))))

(defn next-players-state
  "Returns state updated for the next player."
  [state players']
  {:pre [(map? state)]}
  (merge state {:next-players (vec players')
                :player (first players')}))

(defn init-next-players-state
  "Returns an updated state that sets :next-players to be either:
  :> for forward player order
  :< for reverse player order
  nil for no players"
  [{:keys [player-order] :as state} direction]
  (next-players-state state (case direction
                              :> player-order
                              :< (vec (rseq player-order))
                              nil nil)))

(defn init-next-players!
  "Sets :next-players to be either:
  :> forward player order
  :< reverse player order"
  [state-atom direction]
  (->> (init-next-players-state @state-atom direction)
       (reset! state-atom))
  nil)
