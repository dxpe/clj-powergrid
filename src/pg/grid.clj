(ns pg.grid
  (:require
   [clojure.set :refer [subset?]]
   [pg.constants.grid :refer [region-count]]))

(defn selected-regions-by-city
  "Returns a map of {city region} pairs in specified regions."
  [regions-by-city regions]
  {:pre [(map? regions-by-city) (set? regions)]
   :post [(map? %)]}
  (->> regions-by-city
       (filter (fn [[city region]] (contains? regions region)))
       (into {})))

(defn connection-costs-for-cities
  "Returns a filtered map of connection costs that only include the
  given cities."
  [connection-costs cities]
  {:pre [(map? connection-costs) (set? cities)]}
  (->> connection-costs
       (filter (fn [[two-city-set cost]] (subset? two-city-set cities)))
       (into {})))
