(ns pg.player
  (:refer-clojure :exclude [format])
  (:require
   [clojure.math.combinatorics :as combo]
   [pg.plant :as plant]
   [pg.random :as rand]
   [pg.resource :as resource]
   [pg.utility :as util]))

(defn format
  [player]
  (or player "N/A"))

(defn player-resource-storage-options
  "Returns a sequence of options. Each option is a resource
  map (e.g. {:garbage 6} showing a maximum storage possibility for the
  collection of current player's plants."
  [{:keys [plants plants-by-player player] :as state}]
  (->> (plants-by-player player)
       (plant/plants-resource-storage-options state)))

(defn player-resource-unused-storage-options
  "Returns a sequence of options. Each option is a resource
  map (e.g. {:uranium 0, :garbage 0, :oil 0, :coal 4}) indicating
  unused (i.e available) storage for a player's current power
  plants. This function does not factor in player money or market
  availability of resources."
  [{:keys [plants plants-by-player player resources-by-player] :as state}]
  (let [res (resources-by-player player)]
    (->> (player-resource-storage-options state)
         (map #(resource/subtract-resource-maps % res))
         resource/valid-resource-maps)))

(defn storable-resource-map?
  "Is the provided resource map storable by a player?"
  [{:keys [player resources-by-player] :as state} resource-map]
  (let [rs (resources-by-player player)
        sum (resource/add-resource-maps rs resource-map)
        opts (player-resource-storage-options state)
        xs (map #(resource/subtract-resource-maps %1 %2) opts (repeat sum))]
    (true? (some resource/valid-resource-map? xs))))

(defn player-plant-power-capacity
  "Returns the number of cities the player's plants can power."
  [{:keys [plants-by-player player] :as state}]
  (plant/plants-power-capacity state (plants-by-player player)))

(defn city-count
  "Returns the number of cities owned by the current player."
  [{:keys [cities-by-player player]}]
  (count (cities-by-player player)))
