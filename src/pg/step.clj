(ns pg.step
  (:require
   [pg.constants.city :refer [cities-to-trigger-step-2]]
   [pg.market :as market]
   [pg.utility :refer :all]))

(defn step-2-triggered?
  "After one player builds a certain number of cities (see
  `cities-to-trigger-step-2`), Step 2 does not happen
  immediately (e.g. in the :build-network phase); it starts right before
  the :power-network phase."
  [{:keys [cities-by-player players step] :as state}]
  {:pre [(map? cities-by-player)]}
  (if (= 1 step)
    (let [k (cities-to-trigger-step-2 (count players))]
      (->> cities-by-player
           vals
           (map count)
           (some #(>= % k))))))

(defn start-step-2
  "Start step 2. Returns updated state."
  [{:keys [step] :as state}]
  (print-msg "Starting step 2.")
  (-> state
      (assoc :step 2)))

(defn start-step-3
  "Start step 3. Returns updated state."
  [{:keys [step] :as state}]
  (print-msg "Starting step 3.")
  (-> state
      (assoc :start-step-3? false)
      (assoc :step 3)
      (market/concat-and-split-market)))
