(ns user
  "Tools for interactive development with the REPL. This file should
  not be included in a production build of the application.

  The :refer-clojure clause excludes some parts of clojure.core to make
  auto-completion easier; this is a REPL-based game after all!"
  (:refer-clojure :exclude [long long-array longs
                            short short-array shorts])
  (:require
   [clojure.java.io :as io]
   [clojure.java.javadoc :refer [javadoc]]
   [clojure.math.combinatorics :as combo]
   [clojure.pprint :refer [pp pprint]]
   [clojure.reflect :refer [reflect]]
   [clojure.repl :refer [apropos dir doc find-doc pst source]]
   [clojure.set :as set]
   [clojure.string :as str]
   [clojure.test :refer [run-tests] :as test]
   [clojure.tools.namespace.repl :refer [refresh refresh-all]]
   [pg.choice :as choice]
   [pg.city :as city]
   [pg.deck :as deck]
   [pg.end :as end]
   [pg.game :as game]
   [pg.grid :as grid]
   [pg.grids.usa]
   [pg.market :as market]
   [pg.phase :as phase]
   [pg.plant :as plant]
   [pg.plants.standard :refer [plants]]
   [pg.player :as player]
   [pg.random :as rand]
   [pg.res-costs.standard]
   [pg.res-supply.standard]
   [pg.resource :as resource]
   [pg.revenues.standard]
   [pg.state :as state]
   [pg.turn :as turn]
   [pg.utility :refer [atom?] :as util]))

(def state (atom nil))

(defn init
  []
  (let [players #{:david :john :hannah :michael :austin}
        exclude-regions #{3}
        regions (set/difference pg.grids.usa/regions exclude-regions)
        regions-by-city (grid/selected-regions-by-city
                         pg.grids.usa/regions-by-city regions)
        cities (set (keys regions-by-city))
        cost-by-connection (grid/connection-costs-for-cities
                            pg.grids.usa/connection-costs cities)
        config {:cities cities
                :connections (set (keys cost-by-connection))
                :cost-by-connection cost-by-connection
                :initial-resources pg.res-supply.standard/initial-resources
                :max-resources pg.res-supply.standard/max-resources
                :plants pg.plants.standard/plants
                :players players
                :regions regions
                :regions-by-city regions-by-city
                :resource-costs pg.res-costs.standard/resource-costs
                :resource-supply pg.res-supply.standard/resource-supply
                :revenues pg.revenues.standard/revenues
                :rng (rand/rng 987000)}]
    (reset! state (state/init-state config))
    nil))

(defn print-cmd
  "Prints a command using println."
  [& args]
  (println (str "---> " (apply format args) " <---")))

(defn act!
  [x]
  (game/act! state x))

(defn next-phase!
  []
  (game/next-phase! state))

(defn choices
  []
  (game/choices @state))

(defn long-status
  []
  (game/long-status @state)
  (println))

(defn short-status
  []
  (game/short-status @state)
  (println))

(defn player-info
  []
  (game/player-info @state)
  (println))

(defn resources
  []
  (game/resources @state)
  (println))

(defn start
  []
  (if-not @state
    (throw (ex-info "Run `init` first." {})))
  (game/start! state)
  (next-phase!)
  (long-status))

(defn auction-1
  []
  (act! [:bid 5 :on 5]) ;; :david
  (act! [:bid 6])       ;; :john
  (act! [:pass])        ;; :michael
  (act! [:bid 7])       ;; :austin
  (act! [:pass])        ;; :hannah
  (act! [:bid 8])       ;; :david
  (act! [:pass])        ;; :john
  (act! [:bid 9])       ;; :austin
  (act! [:pass])        ;; :david
  (long-status))

(defn auction-2
  []
  (act! [:bid 8 :on 7]) ;; :david
  (act! [:pass])        ;; :john
  (act! [:pass])        ;; :michael
  (act! [:bid 9])       ;; :hannah
  (act! [:bid 10])      ;; :david
  (act! [:pass])        ;; :hannah
  (long-status))

(defn auction-3
  []
  (act! [:bid 3 :on 3]) ;; :john
  (act! [:bid 4])       ;; :michael
  (act! [:pass])        ;; :hannah
  (act! [:pass])        ;; :john
  (long-status))

(defn auction-4
  []
  (act! [:bid 4 :on 4]) ;; :john
  (act! [:bid 5])       ;; :hannah
  (act! [:pass])        ;; :john
  (long-status))

(defn auction-5
  []
  (act! [:bid 10 :on 10]) ;; :john
  (long-status))

(defn auctions
  []
  (if-not @state
    (throw (ex-info "Run `init` and `start` first." {})))
  (auction-1)
  (auction-2)
  (auction-3)
  (auction-4)
  (auction-5)
  (next-phase!) ;; move to buy-resources phase
  (long-status))

(defn buy-resources
  []
  (act! [:buy {:coal 2}]) ;; hannah
  (act! [:buy {:coal 2}]) ;; austin
  (act! [:buy {:oil 2}])  ;; michael
  (act! [:buy {:coal 2}]) ;; john
  (act! [:buy {:oil 3}])  ;; david
  (next-phase!) ;; move to build-network phase
  (long-status))

(defn build-cities
  []
  (act! [:build :oklahoma-city])
  (act! [:build :atlanta])
  (act! [:build :savannah])
  (act! [:build :san-diego])
  (act! [:build :kansas-city])
  (next-phase!) ;; move to power-network phase
  (long-status))

(defn power-network
  []
  (act! [:power :all]) ;; david
  (act! [:power :all]) ;; john
  (act! [:power :all]) ;; michael
  (act! [:power 5 {:coal 2}]) ;; austin
  (act! [:power :all]) ;; hannah
  (next-phase!)
  (long-status))

(defn resupply
  []
  (next-phase!)
  (long-status))

;; ===== simple bot logic =====

(defn max-resource-costs
  [{:keys [resource-costs] :as state}]
  (->> pg.constants.resource/resources
       (map #(vector % (apply max (vals (resource-costs %)))))
       (into {})))

(defn fake-plant-res-cost
  "Returns a fake 'proxy' price for a plant whose resources are
  currently unavailable. Raises an exception if called with a plant
  driven by renewable resources."
  [{:keys [plants] :as state} plant]
  (let [mrc (max-resource-costs state)
        [[qty kind] _] (plants plant)]
    (* qty (case kind
             :oil     (* 2 (mrc :oil))
             :coal    (* 2 (mrc :coal))
             :hybrid  (+ (mrc :oil) (mrc :coal))
             :garbage (* 2 (mrc :garbage))
             :uranium (* 2 (mrc :uranium))))))

(defn basic-plant-score
  "Returns a score for a plant, independent of price."
  [{:keys [plants] :as state} plant]
  (let [[_ num-cities] (plants plant)
        res-opts (plant/plant-resource-requirement-options state plant)]
    (if (empty? res-opts)
      (* num-cities 2) ;; renewable plant
      (let [costs (map #(resource/resource-map-cost state %) res-opts)
            costs' (map #(or % (fake-plant-res-cost state plant)) costs)
            min-cost (apply min costs')]
        (/ num-cities min-cost)))))

(defn plant-score
  "Returns a score for a plant at a particular price."
  [state plant price]
  {:pre [(number? price)]}
  (/ (basic-plant-score state plant) price))

(defn safe-nth
  [coll index]
  (let [n (count coll)]
    (if (zero? n)
      nil
      (nth coll (if (>= index n)
                  (dec n)
                  index)))))

(defn bot-bid-on-plant?
  "Should a bot bit on a plant?"
  [{:keys [cities-by-player player turn] :as state}]
  (if (= turn 1)
    true
    (let [num-cities (count (cities-by-player player))
          plant-cap (player/player-plant-power-capacity state)]
      (<= plant-cap num-cities))))

(defn bot-auction-start-choice
  [{:keys [market next-players players] :as state} choices]
  (if (bot-bid-on-plant? state)
    (let [sorted-plants (->> market
                             (map #(vector % (plant-score state % %)))
                             (sort-by (comp - second))
                             (map first))
          nnp (count next-players)
          np (count players)
          plant (cond
                 (= nnp np) (safe-nth sorted-plants (+ 1 (rand-int 2)))
                 (>= nnp 3) (safe-nth sorted-plants (rand-int 2))
                 :else (first sorted-plants))
          choice [:bid plant :on plant]]
      (if (some #{choice} choices)
        choice
        [:pass]))
    [:pass]))

(defn bot-auction-raise-choice
  [{:keys [auction-plant highest-bid future-market market]
    :as state} choices]
  (if (bot-bid-on-plant? state)
    (let [combined-market (concat market future-market)
          soon-market (take 6 combined-market)
          price (fn [plant] (if (= plant auction-plant)
                              highest-bid
                              plant))
          plant-vals (->> soon-market
                          (map #(vector % (plant-score state % (price %))))
                          (sort-by (comp - second)))
          sorted-plants (map first plant-vals)
          num-favs (+ 2 (rand-int 2))
          favorite (first sorted-plants)
          favorites (take num-favs sorted-plants)
          choice (if ((set favorites) auction-plant)
                   [:bid (if (= favorite auction-plant)
                           (+ highest-bid 1 (rand-int 3))
                           (inc highest-bid))]
                   [:pass])]
      (if (some #{choice} choices)
        choice
        [:pass]))
    [:pass]))

(defn bot-auction-scrap-choice
  [{:keys [] :as state} choices]
  (->> choices
       (sort-by second)
       first))

(defn bot-auction-plants-choice
  [{:keys [auction-plant discard-resources? scrap-plant?] :as state} choices]
  (cond
   scrap-plant?       (bot-auction-scrap-choice state choices)
   discard-resources? (rand-nth choices)
   auction-plant      (bot-auction-raise-choice state choices)
   :else              (bot-auction-start-choice state choices)))

(defn bot-buy-resource-choice
  [{:keys [plants-by-player player] :as state} choices]
  (let [player-plants (plants-by-player player)
        res-opts (-> (mapcat
                      #(plant/plant-resource-requirement-options state %)
                      player-plants)
                     distinct)
        res-sub-opts (resource/resource-map-options-subsets res-opts)
        buy-opts (map #(vector :buy %) res-sub-opts)
        good-buy-opts (set/intersection (set choices) (set buy-opts))
        quantity (fn [[_ res-map]] (reduce + (vals res-map)))
        sorted-buy-opts (sort-by (comp - quantity) good-buy-opts)]
    (if (seq sorted-buy-opts)
      (rand-nth (take 4 sorted-buy-opts))
      [:pass])))

(defn bot-build-network-choice
  [state choices]
  (rand-nth choices))

(defn bot-power-network-choice
  [state choices]
  (rand-nth choices))

(defn bot-choice
  []
  (let [state @state
        choices (game/choices state)]
    (if (empty? choices)
      (throw (ex-info "No choices. Call next-phase!" {})))
    (case (:phase state)
      :auction-plants (bot-auction-plants-choice state choices)
      :buy-resources  (bot-buy-resource-choice   state choices)
      :build-network  (bot-build-network-choice  state choices)
      :power-network  (bot-power-network-choice  state choices))))

(defn bot-act!
  []
  (let [choice (bot-choice)]
    (print-cmd "%s %s" (:player @state) choice)
    (act! choice)))

(defn bot-act-or-next-phase!
  "Do a random action or go to the next phase."
  []
  (if (empty? (choices))
    (next-phase!)
    (bot-act!)))

;; ===== display/monitoring =====

(defn poll-long-status
  []
  (println)
  (long-status)
  (Thread/sleep 200)
  (recur))

(defn poll-player-info
  []
  (println)
  (player-info)
  (Thread/sleep 200)
  (recur))

;; ===== run the game repeatedly =====

(defn repeat-bot-act-or-next-phase!
  [ms]
  (loop []
    (let [{:keys [deck finishing-order future-market market]} @state]
      (if finishing-order
        (do
          (println "\nFinishing Order: ")
          (doseq [[x n] (map vector finishing-order (iterate inc 1))]
            (println (format "%d : %s" n x))))
        (do
          (bot-act-or-next-phase!)
          (comment
            (println "       market = " market)
            (println "future market = " future-market)
            (println "         deck = " deck))
          (Thread/sleep ms)
          (recur))))))

(comment
  (refresh) (init) (start)
  (auctions) (buy-resources)
  (build-cities) (power-network) (resupply)
  )
(comment
  (refresh) (init) (start) (repeat-bot-act-or-next-phase! 0)
  (repeat-bot-act-or-next-phase! 0)
  )
