(defproject pg "0.1.0-SNAPSHOT"
  :description "Power Grid"
  :url "http://bitbucket.org/davidjames/pg"
  :license {:name "Copyright 2015 David James"}
  :dependencies
  [[org.clojure/clojure "1.7.0-alpha5"]
   [org.clojure/math.combinatorics "0.1.1"]
   [xpe/dijkstra "0.1.0"]]
  :profiles
  {:dev
   {:source-paths ["dev"]
    :dependencies [[org.clojure/tools.namespace "0.2.10"]]
    :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]}})
