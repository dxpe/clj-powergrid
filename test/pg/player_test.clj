(ns pg.player-test
  (:require
   [clojure.test :refer :all]
   [pg.player :as player]
   [pg.test-helper :as helper]))

(defn state-1
  []
  (-> (helper/init-state #{:edison :brush :stanley :siemens} #{1 6})
      (merge {:step 1
              :turn 5
              :player :edison
              :plants-by-player {:edison #{4 8 9}}
              :resources-by-player {:edison {:coal 8 :oil 2}}
              :resources {:coal 8 :oil 8 :garbage 8 :uranium 8}})))

(deftest player-resource-storage-options-test
  (testing "player-resource-storage-options"
    (let [state (state-1)]
      (is (= (player/player-resource-storage-options state)
             [{:oil 2, :coal 10}])))))
