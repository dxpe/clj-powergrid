(ns pg.resource-test
  (:require
   [clojure.test :refer :all]
   [pg.resource :as resource]
   [pg.test-helper :as helper]))

(deftest subtract-resource-maps-test
  (testing "subtract-resource-maps"
    (is (= (resource/subtract-resource-maps
            {:oil 4 :uranium 2}
            {:oil 4})
           {:uranium 2}))
    (is (= (resource/subtract-resource-maps
            {:coal 2}
            {:coal 2})
           {}))
    (is (= (resource/subtract-resource-maps
            {:garbage 2}
            {:garbage 3})
           {:garbage -1}))
    (is (= (resource/subtract-resource-maps
            {:oil 4 :garbage 2}
            {:oil 1 :garbage 3})
           {:oil 3 :garbage -1}))
    (is (= (resource/subtract-resource-maps
            {:oil 4 :uranium 2}
            {:oil 3})
           {:oil 1 :uranium 2}))
    (is (= (resource/subtract-resource-maps
            {}
            {})
           {}))))

(deftest excess-of-test
  (testing "excess-of"
    (is (= (resource/excess-of
            {:oil 4 :uranium 2}
            {:oil 4})
           {:uranium 2}))
    (is (= (resource/excess-of
            {:coal 2}
            {:coal 2})
           {}))
    (is (= (resource/excess-of
            {:garbage 2}
            {:garbage 3})
           {}))
    (is (= (resource/excess-of
            {:oil 4 :garbage 2}
            {:oil 1 :garbage 3})
           {:oil 3}))
    (is (= (resource/excess-of
            {:oil 4 :uranium 2}
            {:oil 3})
           {:oil 1 :uranium 2}))
    (is (= (resource/excess-of
            {}
            {})
           {}))))
