(ns pg.test-helper
  (:require
   [clojure.set :as set]
   [pg.choice :as choice]
   [pg.city :as city]
   [pg.game :as game]
   [pg.grid :as grid]
   [pg.grids.usa]
   [pg.phase :as phase]
   [pg.plant :as plant]
   [pg.plants.standard :refer [plants]]
   [pg.player :as player]
   [pg.random :as rand]
   [pg.res-costs.standard]
   [pg.res-supply.standard]
   [pg.resource :as resource]
   [pg.revenues.standard]
   [pg.state :as state]
   [pg.turn :as turn]))

(defn init-state
  [players exclude-regions]
  {:pre [(set? players) (set? exclude-regions)]}
  (let [regions (set/difference pg.grids.usa/regions exclude-regions)
        regions-by-city (grid/selected-regions-by-city
                         pg.grids.usa/regions-by-city regions)
        cities (set (keys regions-by-city))
        cost-by-connection (grid/connection-costs-for-cities
                            pg.grids.usa/connection-costs cities)
        config {:cities cities
                :regions-by-city regions-by-city
                :connections (set (keys cost-by-connection))
                :cost-by-connection cost-by-connection
                :initial-resources pg.res-supply.standard/initial-resources
                :max-resources pg.res-supply.standard/max-resources
                :plants pg.plants.standard/plants
                :players players
                :regions regions
                :resource-costs pg.res-costs.standard/resource-costs
                :resource-supply pg.res-supply.standard/resource-supply
                :rng (rand/rng 987000)
                :revenues pg.revenues.standard/revenues}]
    (state/init-state config)))
