(ns pg.city-test
  (:require
   [clojure.test :refer :all]
   [pg.city :as city]
   [pg.test-helper :as helper]))

(defn state-1
  []
  (-> (helper/init-state #{:david :john :hannah :michael :austin} #{3})
      (assoc-in [:cities-by-player :david]   #{:dallas})
      (assoc-in [:cities-by-player :john]    #{:houston})
      (assoc-in [:cities-by-player :michael] #{:phoenix})
      (assoc-in [:players-by-city :dallas]  #{:david})
      (assoc-in [:players-by-city :houston] #{:john})
      (assoc-in [:players-by-city :phoenix] #{:michael})))

(deftest david-step-1-test
  (testing "david in step 1"
    (let [state (-> (state-1)
                    (merge {:player :david :step 1}))]
      (is (= (city/unoccupied-cities state)
             #{:jacksonville :norfolk :fargo :san-diego :atlanta :salt-lake-city
               :cheyenne :los-angeles :raleigh :seattle :denver :st-louis
               :las-vegas :boise :minneapolis :billings :memphis :chicago
               :cincinatti :knoxville :portland :kansas-city :oklahoma-city
               :birmingham :san-francisco :tampa :duluth :santa-fe :new-orleans
               :savannah :miami :omaha}))
      (is (= (city/available-cities state)
             #{:jacksonville :norfolk :fargo :san-diego :atlanta :salt-lake-city
               :cheyenne :los-angeles :raleigh :seattle :denver :st-louis
               :las-vegas :boise :minneapolis :billings :memphis :chicago
               :cincinatti :knoxville :portland :kansas-city :oklahoma-city
               :birmingham :san-francisco :tampa :duluth :santa-fe :new-orleans
               :savannah :miami :omaha}))
      (is (= (city/connected-cities state)
             #{:dallas :houston}))
      (is (= (city/candidate-cities state)
             #{:memphis :oklahoma-city :santa-fe :new-orleans}))
      (is (= (city/city-connection-cost state :memphis)
             12))
      (is (= (city/city-connection-cost state :oklahoma-city)
             3))
      (is (= (city/city-connection-cost state :santa-fe)
             16))
      (is (= (city/city-connection-cost state :new-orleans)
             12)))))

(deftest david-step-2-test
  (testing "david in step 2"
    (let [state (-> (state-1)
                    (merge {:player :david :step 2}))]
      (is (= (city/unoccupied-cities state)
             #{:jacksonville :norfolk :fargo :san-diego :atlanta :salt-lake-city
               :cheyenne :los-angeles :raleigh :seattle :denver :st-louis
               :las-vegas :boise :minneapolis :billings :memphis :chicago
               :cincinatti :knoxville :portland :kansas-city :oklahoma-city
               :birmingham :san-francisco :tampa :duluth :santa-fe :new-orleans
               :savannah :miami :omaha}))
      (is (= (city/available-cities state)
             #{:jacksonville :norfolk :fargo :san-diego :atlanta :salt-lake-city
               :cheyenne :los-angeles :houston :raleigh :seattle :denver
               :st-louis :las-vegas :phoenix :boise :minneapolis :billings
               :memphis :chicago :cincinatti :knoxville :portland :kansas-city
               :oklahoma-city :birmingham :san-francisco :tampa :duluth
               :santa-fe :new-orleans :savannah :miami :omaha}))
      (is (= (city/connected-cities state)
             #{:dallas :houston}))
      (is (= (city/candidate-cities state)
             #{:houston :memphis :oklahoma-city :santa-fe :new-orleans})))))

(deftest john-step-1-test
  (testing "john in step 1"
    (let [state (-> (state-1)
                    (merge {:player :john :step 1}))]
      (is (= (city/unoccupied-cities state)
             #{:jacksonville :norfolk :fargo :san-diego :atlanta :salt-lake-city
               :cheyenne :los-angeles :raleigh :seattle :denver :st-louis
               :las-vegas :boise :minneapolis :billings :memphis :chicago
               :cincinatti :knoxville :portland :kansas-city :oklahoma-city
               :birmingham :san-francisco :tampa :duluth :santa-fe :new-orleans
               :savannah :miami :omaha}))
      (is (= (city/available-cities state)
             #{:jacksonville :norfolk :fargo :san-diego :atlanta :salt-lake-city
               :cheyenne :los-angeles :raleigh :seattle :denver :st-louis
               :las-vegas :boise :minneapolis :billings :memphis :chicago
               :cincinatti :knoxville :portland :kansas-city :oklahoma-city
               :birmingham :san-francisco :tampa :duluth :santa-fe :new-orleans
               :savannah :miami :omaha}))
      (is (= (city/connected-cities state)
             #{:dallas :houston}))
      (is (= (city/candidate-cities state)
             #{:memphis :oklahoma-city :santa-fe :new-orleans}))
      (is (= (city/city-connection-cost state :memphis)
             17))
      (is (= (city/city-connection-cost state :oklahoma-city)
             8))
      (is (= (city/city-connection-cost state :santa-fe)
             21))
      (is (= (city/city-connection-cost state :new-orleans)
             8)))))

(deftest john-step-2-test
  (testing "john in step 2"
    (let [state (-> (state-1)
                    (merge {:player :john :step 2}))]
      (is (= (city/connected-cities state)
             #{:dallas :houston}))
      (is (= (city/candidate-cities state)
             #{:dallas :memphis :oklahoma-city :santa-fe :new-orleans})))))

(deftest michael-step-1-test
  (testing "michael in step 1"
    (let [state (-> (state-1)
                    (merge {:player :michael :step 1}))]
      (is (= (city/connected-cities state)
             #{:phoenix}))
      (is (= (city/candidate-cities state)
             #{:san-diego :las-vegas :santa-fe})))))

(deftest michael-step-2-test
  (testing "michael in step 2"
    (let [state (-> (state-1)
                    (merge {:player :michael :step 2}))]
      (is (= (city/connected-cities state)
             #{:phoenix}))
      (is (= (city/candidate-cities state)
             #{:san-diego :las-vegas :santa-fe})))))

(deftest directed-edges-test
  (testing "directed-edges"
    (is (= (city/directed-edges {#{:a :b} 10
                                 #{:b :c} 6})
           {[:b :a] 10
            [:a :b] 10
            [:c :b] 6
            [:b :c] 6}))))

(deftest relevant-connection-costs-test
  (testing "relevant-connection-costs"
    (let [cc {#{:a :b} 9
              #{:a :c} 8
              #{:a :g} 11
              #{:b :c} 7
              #{:b :e} 6
              #{:c :d} 5
              #{:c :e} 4
              #{:d :e} 3
              #{:d :g} 12
              #{:e :f} 2
              #{:e :h} 13
              #{:f :h} 14}]
      (is (= (city/relevant-connection-costs cc #{:c :e} :a)
             {#{:c :a} 8
              #{:e :c} 4}))
      (is (= (city/relevant-connection-costs cc #{:c :e} :b)
             {#{:e :c} 4
              #{:e :b} 6
              #{:c :b} 7}))
      (is (= (city/relevant-connection-costs cc #{:c :e} :d)
             {#{:c :d} 5
              #{:e :c} 4
              #{:e :d} 3}))
      (is (= (city/relevant-connection-costs cc #{:c :e :d} :a)
             {#{:c :d} 5
              #{:c :a} 8
              #{:e :c} 4
              #{:e :d} 3}))
      (is (= (city/relevant-connection-costs cc #{:c :e :d} :g)
             {#{:c :d} 5
              #{:e :c} 4
              #{:g :d} 12
              #{:e :d} 3}))
      (is (= (city/relevant-connection-costs cc #{:c :e :d} :d)
             {#{:c :d} 5
              #{:e :c} 4
              #{:e :d} 3}))
      (is (= (city/relevant-connection-costs cc #{:c :e :d} :h)
             {#{:c :d} 5
              #{:e :h} 13
              #{:e :c} 4
              #{:e :d} 3}))
      (is (= (city/relevant-connection-costs cc #{:c :e :d} :f)
             {#{:c :d} 5
              #{:e :f} 2
              #{:e :c} 4
              #{:e :d} 3})))))

(defn state-2
  []
  (-> (helper/init-state #{:jason :victor :tom :eddie} #{3 4})
      (assoc-in [:cities-by-player :jason]  #{:dallas})
      (assoc-in [:cities-by-player :victor] #{:houston})
      (assoc-in [:cities-by-player :tom]    #{:memphis})
      (assoc-in [:cities-by-player :eddie]  #{:san-diego})
      (assoc-in [:players-by-city :dallas]    #{:jason})
      (assoc-in [:players-by-city :houston]   #{:victor})
      (assoc-in [:players-by-city :memphis]   #{:tom})
      (assoc-in [:players-by-city :san-diego] #{:eddie})))

(deftest victor-step-1-test
  (testing "victor in step 1"
    (let [state (-> (state-2)
                    (merge {:player :victor :step 1}))]
      (is (= (city/connected-cities state)
             #{:dallas :houston :memphis}))
      (is (= (city/candidate-cities state)
             #{:st-louis :kansas-city :oklahoma-city :birmingham
               :santa-fe :new-orleans}))
      (is (= (city/city-connection-cost state :st-louis)
             24 ;; not 22
             ))
      (is (= (city/city-connection-cost state :kansas-city)
             29 ;; not 16 [:oklahoma-city] or 27 [:kansas-city]
             ))
      (is (= (city/city-connection-cost state :oklahoma-city)
             8))
      (is (= (city/city-connection-cost state :birmingham)
             23 ;; not 19 [:new-orleans] or 21 [:new-orleans :memphis]
             ))
      (is (= (city/city-connection-cost state :santa-fe)
             21))
      (is (= (city/city-connection-cost state :new-orleans)
             8)))))
