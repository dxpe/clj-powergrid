(ns pg.utility-test
  (:require
   [clojure.test :refer :all]
   [pg.utility :refer :all]))

(deftest safe-merge-test
  (testing "safe-merge"
    (is (= (safe-merge {:a 1 :b 2} {:c 3 :d 4})
           (merge {:a 1 :b 2} {:c 3} {:d 4})))))
