(ns pg.phases.auction-plants-test
  (:require
   [clojure.test :refer :all]
   [pg.phases.auction-plants :as ap]
   [pg.player :as player]
   [pg.test-helper :as helper]))

(defn state-1
  "Return a game state based on the following activity.

  For the purposes of this test case, I focus on plant
  accumulation. The details of cities, resources, and
  money are approximated -- or left out entirely, since
  they do not affect this particular test.

  Starting deck:
  [13 46 27 17 26 23 42 21 24 34
   50 40 12 31 38 22 20 28 44 16
   18 30 33 14 35 32 36 37 39 19
   25 11 29 15 :step-3]

  Turn 1:
  top of deck: [13 46 27 17 26 23]
  market: [ 3  4  5  6]
  future: [ 7  8  9 10]
  :edison  buys  4 own #{ 4} draw: 13
  :brush   buys  5 own #{ 5} draw: 46
  :stanley buys  3 own #{ 3} draw: 27
  :siemens buys  7 own #{ 7} draw: 17
                             bury: 46
                             draw: 26
  bottom of deck: [:step-3 46]
  top of deck: [23 42 21 24 34 50]

  Turn 2:
  market: [ 6  8  9 10]
  future: [13 17 26 27]
  :stanley pass   own #{ 3   }
  :edison  buy  8 own #{ 4  8} draw: 23
  :siemens pass   own #{ 7   }
  :brush   buy 13 own #{ 5 13} draw: 42
                               bury: 27
                               draw: 21
  bottom of deck: [:step-3 46 27]
  top of deck: [24 34 50 40 12 31]

  Turn 3:
  market: [ 6  9 10 17]
  future: [21 23 26 42]
  :siemens pass   own #{ 7   }
  :brush   pass   own #{ 5 13}
  :edison  pass   own #{ 4  8}
  :stanley pass   own #{ 3   }
                            discard:  6
                               draw: 24
                               bury: 42
                               draw: 34
  bottom of deck: [:step-3 46 27 42]
  top of deck: [50 40 12 31 38 22]

  Turn 4:
  market: [ 9 10 17 21]
  future: [23 24 26 34]
  :brush   pass   own #{ 5 13   }
  :edison  buy  9 own #{ 4  8  9} draw: 50
  :siemens buy 23 own #{ 7 23   } draw: 40
  :stanley buy 21 own #{ 3 21   } draw: 12
                                  bury: 50
                                  draw: 31
  bottom of deck: [:step-3 46 27 42 50]
  top of deck: [38 22 20 28 44 16]

  Turn 5:
  market: [10 12 17 24]
  future: [26 31 34 40]

  Now, it is Brush's turn.

  Current deck:
  [38 22 20 28 44 16 18 30 33 14
   35 32 36 37 39 19 25 11 29 15
   :step-3 46 27 42 40]"
  []
  (-> (helper/init-state #{:edison :brush :stanley :siemens} #{1 6})
      (merge {:step 1
              :turn 5
              :player-order [:brush :edison :siemens :stanley]
              :phase :auction-plants
              :player :brush
              :next-players [:brush :edison :siemens :stanley]
              :auction-players [:brush :edison :siemens :stanley]
              :auction-plant nil
              :highest-bid nil
              :highest-bidder nil
              :scrap-plant? false
              :discard-resources? false
              :market [10 12 17 24]
              :future-market [26 31 34 40]
              :deck [38 22 20 28 44 16 18 30 33 14
                     35 32 36 37 39 19 25 11 29 15
                     :step-3 46 27 42 40]
              :cities-by-player {}
              :players-by-city {}
              :money-by-player {:edison  39
                                :brush   43
                                :stanley 48
                                :siemens 53}
              :plants-by-player {:edison  #{4 8 9}
                                 :brush   #{5 13}
                                 :stanley #{3 21}
                                 :siemens #{7 23}}
              ;; This part will be adjusted below
              :resources-by-player {:edison  {}
                                    :brush   {}
                                    :stanley {}
                                    :siemens {}}
              :resources {:coal 8 :oil 8 :garbage 8 :uranium 8}})))

(deftest auction-example-1-test
  (testing "auction-plants"
    (let [state (state-1)

          _ (is (= (:player state) :brush))
          _ (is (= (get-in state [:plants-by-player :brush]) #{13 5}))
          _ (is (= (get-in state [:money-by-player :brush]) 43))
          _ (is (= (count (ap/choices state)) 114))
          state (ap/effect state [:bid 10 :on 10]) ;; :brush

          _ (is (= (:player state) :edison))
          _ (is (= (:next-players state) [:edison :siemens :stanley :brush]))
          _ (is (= (:auction-players state) [:brush :edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 10))
          _ (is (= (:highest-bid state) 10))
          _ (is (= (:highest-bidder state) :brush))
          _ (is (= (count (ap/choices state)) 30))
          state (ap/effect state [:pass]) ;; :edison

          _ (is (= (:player state) :siemens))
          _ (is (= (:next-players state) [:siemens :stanley :brush]))
          _ (is (= (:auction-players state) [:brush :edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 10))
          _ (is (= (:highest-bid state) 10))
          _ (is (= (:highest-bidder state) :brush))
          _ (is (= (count (ap/choices state)) 44))
          state (ap/effect state [:pass]) ;; :siemens

          _ (is (= (:player state) :stanley))
          _ (is (= (:next-players state) [:stanley :brush]))
          _ (is (= (:auction-players state) [:brush :edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 10))
          _ (is (= (:highest-bid state) 10))
          _ (is (= (:highest-bidder state) :brush))
          _ (is (= (count (ap/choices state)) 39))
          state (ap/effect state [:pass]) ;; :stanley

          ;; <<<< :brush wins plant 10 for 10 Elektro >>>>

          ;; verify Brush's plant purchase
          _ (is (= (:market state) [12 17 24 26]))
          _ (is (= (:future-market state) [31 34 38 40]))
          _ (is (= (get-in state [:plants-by-player :brush]) #{13 5 10}))
          _ (is (= (get-in state [:money-by-player :brush]) 33))

          ;; Edison's turn to start the auction
          _ (is (= (:player state) :edison))
          _ (is (= (get-in state [:plants-by-player :edison]) #{4 9 8}))
          _ (is (= (get-in state [:money-by-player :edison]) 39))

          _ (is (= (:next-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) nil))
          _ (is (= (:highest-bid state) nil))
          _ (is (= (:highest-bidder state) nil))
          _ (is (= (count (ap/choices state)) 82))
          state (ap/effect state [:bid 18 :on 17]) ;; :edison

          _ (is (= (:player state) :siemens))
          _ (is (= (:next-players state) [:siemens :stanley :edison]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 18))
          _ (is (= (:highest-bidder state) :edison))
          _ (is (= (count (ap/choices state)) 36))
          state (ap/effect state [:bid 19]) ;; :siemens

          _ (is (= (:player state) :stanley))
          _ (is (= (:next-players state) [:stanley :edison :siemens]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 19))
          _ (is (= (:highest-bidder state) :siemens))
          _ (is (= (count (ap/choices state)) 30))
          state (ap/effect state [:bid 21]) ;; :stanley

          _ (is (= (:player state) :edison))
          _ (is (= (:next-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 21))
          _ (is (= (:highest-bidder state) :stanley))
          _ (is (= (count (ap/choices state)) 19))
          state (ap/effect state [:bid 22]) ;; edison

          _ (is (= (:player state) :siemens))
          _ (is (= (:next-players state) [:siemens :stanley :edison]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 22))
          _ (is (= (:highest-bidder state) :edison))
          _ (is (= (count (ap/choices state)) 32))
          state (ap/effect state [:pass]) ;; siemens

          _ (is (= (:player state) :stanley))
          _ (is (= (:next-players state) [:stanley :edison]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 22))
          _ (is (= (:highest-bidder state) :edison))
          _ (is (= (count (ap/choices state)) 27))
          state (ap/effect state [:bid 23]) ;; stanley

          _ (is (= (:player state) :edison))
          _ (is (= (:next-players state) [:edison :stanley]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 23))
          _ (is (= (:highest-bidder state) :stanley))
          _ (is (= (count (ap/choices state)) 17))
          state (ap/effect state [:bid 24]) ;; edison

          _ (is (= (:player state) :stanley))
          _ (is (= (:next-players state) [:stanley :edison]))
          _ (is (= (:auction-players state) [:edison :siemens :stanley]))
          _ (is (= (:auction-plant state) 17))
          _ (is (= (:highest-bid state) 24))
          _ (is (= (:highest-bidder state) :edison))
          _ (is (= (count (ap/choices state)) 25))]
      (comment
        "Here, a plant must be scrapped, but resources are not discarded.")
      (let [state (assoc-in state [:resources-by-player :edison]
                            {:coal 4 :oil 2})
            state (ap/effect state [:pass]) ;; stanley

            ;; <<<< :edison wins plant 17 for 24 Elektro >>>>
            ;; <<<< :edison must scrap a plant >>>>

            _ (is (= (:player state) :edison))
            _ (is (= (:next-players state) [:edison]))
            _ (is (= (:auction-players state) [:edison :siemens :stanley]))
            _ (is (= (:auction-plant state) 17))
            _ (is (= (:highest-bid state) 24))
            _ (is (= (:highest-bidder state) :edison))
            _ (is (= (:scrap-plant? state) true))
            _ (is (= (:discard-resources? state) false))
            _ (is (= (:market state) [12 17 24 26]))
            _ (is (= (:future-market state) [31 34 38 40]))
            _ (is (= (get-in state [:plants-by-player :edison]) #{4 9 8}))
            _ (is (= (get-in state [:money-by-player :edison]) 39))
            _ (is (= (ap/choices state)
                     (list [:scrap 4] [:scrap 9] [:scrap 8])))
            _ (is (thrown? clojure.lang.ExceptionInfo
                           (ap/effect state [:scrap 6])))
            state (ap/effect state [:scrap 4]) ;; edison

            _ (is (= (:player state) :siemens))
            _ (is (= (:next-players state) [:siemens :stanley]))
            _ (is (= (:auction-players state) [:siemens :stanley]))
            _ (is (= (:auction-plant state) nil))
            _ (is (= (:highest-bid state) nil))
            _ (is (= (:highest-bidder state) nil))
            _ (is (= (:scrap-plant? state) false))
            _ (is (= (:discard-resources? state) false))
            _ (is (= (:market state) [12 22 24 26]))
            _ (is (= (:future-market state) [31 34 38 40]))
            _ (is (= (get-in state [:plants-by-player :edison]) #{17 9 8}))
            _ (is (= (get-in state [:money-by-player :edison]) 15))
            _ (is (= (get-in state [:resources-by-player :edison])
                     {:coal 4 :oil 2}))])
      (comment
        "Here, a plant must be scrapped, and resources must be discarded.")
      (let [state (assoc-in state [:resources-by-player :edison]
                            {:coal 8 :oil 2})
            state (ap/effect state [:pass]) ;; stanley

            ;; <<<< :edison wins plant 17 for 24 Elektro >>>>
            ;; <<<< :edison must scrap a plant >>>>

            _ (is (= (:player state) :edison))
            _ (is (= (:next-players state) [:edison]))
            _ (is (= (:auction-players state) [:edison :siemens :stanley]))
            _ (is (= (:auction-plant state) 17))
            _ (is (= (:highest-bid state) 24))
            _ (is (= (:highest-bidder state) :edison))
            _ (is (= (:scrap-plant? state) true))
            _ (is (= (:discard-resources? state) false))
            _ (is (= (:market state) [12 17 24 26]))
            _ (is (= (:future-market state) [31 34 38 40]))
            _ (is (= (get-in state [:plants-by-player :edison]) #{4 9 8}))
            _ (is (= (ap/choices state)
                     (list [:scrap 4] [:scrap 9] [:scrap 8])))
            _ (is (thrown? clojure.lang.ExceptionInfo
                           (ap/effect state [:scrap 6])))
            state (ap/effect state [:scrap 4]) ;; edison

            ;; <<<< :edison must discard resources >>>>

            _ (is (= (:player state) :edison))
            _ (is (= (:next-players state) [:edison]))
            _ (is (= (:auction-players state) [:siemens :stanley]))
            _ (is (= (:auction-plant state) nil))
            _ (is (= (:highest-bid state) nil))
            _ (is (= (:highest-bidder state) nil))
            _ (is (= (:scrap-plant? state) false))
            _ (is (= (:discard-resources? state) true))
            _ (is (= (:market state) [12 22 24 26]))
            _ (is (= (:future-market state) [31 34 38 40]))
            _ (is (= (get-in state [:plants-by-player :edison]) #{17 9 8}))
            _ (is (= (get-in state [:money-by-player :edison]) 15))
            _ (is (= (get-in state [:resources-by-player :edison])
                     {:coal 8 :oil 2}))
            _ (is (= (ap/choices state)
                     (list [:discard {:coal 2}])))
            _ (is (thrown? clojure.lang.ExceptionInfo
                           (ap/effect state [:discard {:coal 1}])))
            _ (is (thrown? clojure.lang.ExceptionInfo
                           (ap/effect state [:discard {:coal 3}])))
            state (ap/effect state [:discard {:coal 2}]) ;; edison

            _ (is (= (:player state) :siemens))
            _ (is (= (:next-players state) [:siemens :stanley]))
            _ (is (= (:auction-players state) [:siemens :stanley]))
            _ (is (= (:auction-plant state) nil))
            _ (is (= (:highest-bid state) nil))
            _ (is (= (:highest-bidder state) nil))
            _ (is (= (:scrap-plant? state) false))
            _ (is (= (:discard-resources? state) false))
            _ (is (= (:market state) [12 22 24 26]))
            _ (is (= (:future-market state) [31 34 38 40]))
            _ (is (= (get-in state [:plants-by-player :edison]) #{17 9 8}))
            _ (is (= (get-in state [:money-by-player :edison]) 15))
            _ (is (= (get-in state [:resources-by-player :edison])
                     {:oil 2 :coal 6}))]))))
