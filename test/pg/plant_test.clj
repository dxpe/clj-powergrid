(ns pg.plant-test
  (:require
   [clojure.test :refer :all]
   [pg.plant :as plant]
   [pg.test-helper :as helper]))

(defn state-1
  []
  (-> (helper/init-state #{:king :gandhi :mandela} #{2 3 4})))

(deftest plant-resource-storage-options-test
  (testing "plant-resource-storage-options"
    (let [state (state-1)]
      (is (= (plant/plant-resource-storage-options state 4)
             [{:coal 4}]))
      (is (= (plant/plant-resource-storage-options state 5)
             [{:coal 4}
              {:oil 1, :coal 3}
              {:oil 2, :coal 2}
              {:oil 3, :coal 1}
              {:oil 4}]))
      (is (= (plant/plant-resource-storage-options state 6)
             [{:garbage 2}]))
      (is (= (plant/plant-resource-storage-options state 7)
             [{:oil 6}]))
      (is (= (plant/plant-resource-storage-options state 12)
             [{:coal 4}
              {:oil 1, :coal 3}
              {:oil 2, :coal 2}
              {:oil 3, :coal 1}
              {:oil 4}]))
      (is (= (plant/plant-resource-storage-options state 13)
             []))
      (is (= (plant/plant-resource-storage-options state 22)
             [])))))

(deftest plants-resource-storage-options-test
  (testing "plants-resource-storage-options"
    (let [state (state-1)]
      ;; #4:[2 :coal]->1
      (is (= (plant/plants-resource-storage-options state #{4})
             [{:coal 4}]))
      ;; #4:[2 :coal]->1 #6:[1 :garbage]->1
      (is (= (plant/plants-resource-storage-options state #{4 6})
             [{:garbage 2, :coal 4}]))
      ;; #4:[2 :coal]->1 #6:[1 :garbage]->1 #7:[3 :oil]->2
      (is (= (plant/plants-resource-storage-options state #{4 6 7})
             [{:garbage 2, :oil 6, :coal 4}]))
      ;; #5:[2 :hybrid]->1
      (is (= (plant/plants-resource-storage-options state #{5})
             [{:coal 4}
              {:oil 1, :coal 3}
              {:oil 2, :coal 2}
              {:oil 3, :coal 1}
              {:oil 4}]))
      ;; #5:[2 :hybrid]->1 #6:[1 :garbage]->1
      (is (= (plant/plants-resource-storage-options state #{5 6})
             [{:garbage 2, :coal 4}
              {:garbage 2, :oil 1, :coal 3}
              {:garbage 2, :oil 2, :coal 2}
              {:garbage 2, :oil 3, :coal 1}
              {:garbage 2, :oil 4}]))
      ;; #5:[2 :hybrid]->1 #12:[2 :hybrid]->2
      (is (= (plant/plants-resource-storage-options state #{5 12})
             [{:coal 8}
              {:oil 1, :coal 7}
              {:oil 2, :coal 6}
              {:oil 3, :coal 5}
              {:oil 4, :coal 4}
              {:oil 5, :coal 3}
              {:oil 6, :coal 2}
              {:oil 7, :coal 1}
              {:oil 8}]))
      ;; #13:[]->1
      (is (= (plant/plants-resource-storage-options state #{13})
             []))
      ;; #5:[2 :hybrid]->1 #13:[]->1
      (is (= (plant/plants-resource-storage-options state #{5 13})
             [{:coal 4}
              {:oil 1, :coal 3}
              {:oil 2, :coal 2}
              {:oil 3, :coal 1}
              {:oil 4}]))
      ;; #13:[]->1 #22:[]->2
      (is (= (plant/plants-resource-storage-options state #{13 22})
             [])))))

(deftest too-many-resources?-test
  (testing "too-many-resources"
    (let [f #(-> (state-1)
                 (assoc :player :king)
                 (assoc-in [:resources-by-player :king] %))]
      (is (= (plant/too-many-resources?
              (f {:coal 4}) #{5})
             false))
      (is (= (plant/too-many-resources?
              (f {:coal 3 :oil 1}) #{5})
             false))
      (is (= (plant/too-many-resources?
              (f {:coal 3 :oil 2}) #{5})
             true)))))

(deftest excess-resources-options-test
  (testing "excess-resources-options"
    (let [f #(-> (state-1)
                 (assoc :player :king)
                 (assoc-in [:resources-by-player :king] %))]
      ;; #5:[2 :hybrid]->1
      (is (= (plant/excess-resources-options
              (f {:coal 4}) #{5})
             []))
      ;; #5:[2 :hybrid]->1
      (is (= (plant/excess-resources-options
              (f {:coal 3 :oil 1}) #{5})
             []))
      ;; #5:[2 :hybrid]->1
      (is (= (plant/excess-resources-options
              (f {:coal 3 :oil 2}) #{5})
             [{:oil 1}
              {:coal 1}]))
      ;; #5:[2 :hybrid]->1
      (is (= (plant/excess-resources-options
              (f {:coal 3 :oil 3}) #{5})
             [{:oil 2}
              {:oil 1, :coal 1}
              {:coal 2}]))
      ;; #13:[]->1
      (is (= (plant/excess-resources-options
              (f {:garbage 1}) #{13})
             [{:garbage 1}]))
      ;; #5:[2 :hybrid]->1 #13:[]->1
      (is (= (plant/excess-resources-options
              (f {:uranium 1}) #{5 13})
             [{:uranium 1}]))
      ;; #13:[]->1 #28:[1 :uranium]->4
      (is (= (plant/excess-resources-options
              (f {:garbage 1}) #{13 28})
             [{:garbage 1}]))
      ;; #13:[]->1 #28:[1 :uranium]->4
      (is (= (plant/excess-resources-options
              (f {:oil 1 :coal 1}) #{13 28})
             [{:oil 1 :coal 1}]))
      ;; #13:[]->1 #18:[]->2
      (is (= (plant/excess-resources-options
              (f {:uranium 2}) #{13 18})
             [{:uranium 2}]))
      ;; #13:[]->1 #18:[]->2 #27:[]->3
      (is (= (plant/excess-resources-options
              (f {:coal 2}) #{13 18 27})
             [{:coal 2}])))))
